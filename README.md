# ABC interpreter

This repository contains the following:

- The toolset used by [`clm`](https://clean-lang.org/pkg/base-clm) to generate
  bytecode from Clean's intermediate language ABC.
- A C interpreter to run this bytecode independent from a native Clean runtime.
- A WebAssembly interpreter to run this bytecode in the browser (or other Wasm
  runtimes).
- A Clean library to lazily de/serialize arbitrary Clean expressions (including
  functions), collecting the accompanying bytecode, and evaluate and interpret
  the expression on a different machine (or in WebAssembly).

Most notably this is used in [iTasks](https://clean-lang.org/pkg/itasks) to run
functional editor logic in the browser.

This system was presented at Implementation and application of Functional
Languages (IFL) 2019 by Camil Staps, John van Groningen, and Rinus Plasmeijer.
[doi:10.1145/3412932.3412941](https://doi.org/10.1145/3412932.3412941);
[PDF](https://camilstaps.nl/assets/pdf/ifl2019.pdf).

## Usage

To generate bytecode for the interpreter, add the following to your
`nitrile.yml`:

```yml
dependencies:
  abc-interpreter: ... # the current version
clm_options:
  bytecode: true
```

If you want to generate bytecode for the WebAssembly interpreter, use
`bytecode: prelinked` instead.

By default, ABC code will be optimized for use in the interpreter. If you do
not want this, use a record:

```yml
clm_options:
  bytecode:
    optimize_abc: false
    prelink: true
```

When `bytecode` is given (and not `null`), `clm` will generate a file
`target.bc` alongside `target.exe` / `target`. Prelinked bytecode is generated
as `target.pbc`.

When using the Clean library for de/serialization of Clean expressions, you
should also set the following options:

```yml
clm_options:
  generate_descriptors: true
  export_local_labels: true
  strip: false
```

See the documentation in [ABC.Interpreter](/lib/ABC/Interpreter.dcl) for more
details about the serialization library itself.

### Independent interpretation and debugging

The interpreter can also be used stand-alone to run Clean programs.
`bin/abci-interpret` is included in the nitrile package and can be used to
interpret any bytecode file:

```bash
abci-interpret path/to/application.bc
```

On Linux systems there is also a graphical debugger, inspired by `gdb`:

```bash
abci-debug path/to/application.bc
```

For help on the interface, press <kbd>?</kbd> in the debugger GUI.

## Maintainer, authors, copyright & license

This project is maintained by [Camil Staps][].

Copyright is held by the authors of the individual commits. Use `git blame` or
the GitLab blame view to determine the copyright holder on specific parts of
the code base. Contributors include:

- Gijs Alberts
- Elroy Jumpertz
- Bas Lijnse
- Mart Lubbers
- Steffen Michels
- Camil Staps
- Erin van der Veen

[Camil Staps]: https://camilstaps.nl/
