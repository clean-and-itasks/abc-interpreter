# Debugging the JavaScript interface

Debugging code using the JavaScript interface in `ABC.Interpreter.JavaScript`
is notoriously hard. The below is a list of things you can do to gain more
information, roughly in order of what gives more (and more low-level)
information, but also of what requires more effort on your part.

## `jsTrace`, `jsTraceVal`, and `trace_stdout`

These work like similar functions in `StdDebug`: they print a value to the
console while evaluating another value. A value of type `JSVal` can be traced
with either function. With `jsTrace`, you will see a string representation of
the value as the Clean world knows it; with `jsTraceVal` you will see the
actual value in the JavaScript side.

Because `trace_stdout` in `Debug.Trace` simply writes to `stdout` like the
Clean run-time system does when evaluating a `Start` rule, it can also be used
in the WebAssembly interpreter. This can be useful when there is no `toString`
instance or when it is not appropriate. Be aware however that `trace_stdout`
will (1) do a hyperstrict evaluation while printing the value, which may not
finish or at the very least alter the execution order, and (2) may print an
infinite amount of data in case of an infinite or cyclic data structure, which
may crash the browser.

## `ABC_DEBUG`

You can set `ABC_DEBUG=true` at the top of `abc-interpreter.js`. You can find
this file in `$CLEAN_HOME/lib/ABCInterpreter/WebPublic/js`. Setting this to
true will print everything that happens in the JavaScript interface:
evaluations, assignments, etc.

### `ABC.js`

You will see references to things like `ABC.js[i]` where `i` is some integer.
These are values that are stored on the JavaScript side because they cannot be
copied to Clean: objects and functions. You can access them from the console as
`itasks.ABC.js[i]` to inspect their value.

Because indices in `ABC.js` are generally assigned in ascending order (this may
change temporarily after a garbage collection cycle), you can often figure out
the values just by looking at the context. For example, here is the
initialisation sequence of a Pikaday date input field in iTasks:

```js
 1	ABC.js[0]['initDOMEl'] .= ABC.ap(1)
 2	ABC.js[0]['beforeRemove'] .= ABC.ap(2)
 3	eval ABC.js[0].attributes.value
 4	eval ABC.js[0]['domEl']
 5	ABC.js[1]['value'] .= null
 6	eval {}
 7	ABC.js[2]['field'] .= ABC.js[1]
 8	ABC.js[2]['format'] .= 'YYYY-MM-DD'
 9	ABC.js[2]['firstDay'] .= 1
10	ABC.js[2]['onSelect'] .= ABC.ap(4)
11	eval ABC.js[1]['addEventListener']('keyup',ABC.ap(5))
12	eval new Pikaday(ABC.js[2])
13	ABC.js[0]['picker'] .= ABC.js[3]
14	ABC.js[0]['onAttributeChange'] .= ABC.ap(6)
```

First note that lines 1, 2, 5, 7–10, 13, and 14 are assignments. In these cases
no value is passed from JavaScript to Clean, so `ABC.js` remains untouched.

The first value, `ABC.js[0]`, is the `JSVal` argument to the first parameter of
a function wrapped with `wrapInitFunction`. In an iTasks setting, this is the
iTasks component (for Pikaday, a text field).

Candidates for the value `ABC.js[2]` are therefore line 3 and 4, which obtain a
value and a DOM element. One can guess (especially when looking at the code of
the Pikaday library) that the `value` is a string, which can be copied to
Clean. Therefore, `ABC.js` remains untouched in line 3 as well, and `ABC.js[1]`
must refer to `ABC.js[0].domEl`.

Likewise one can figure out that `ABC.js[2]` is created in line 6 and
`ABC.js[3]` in line 12. Line 11 is presumably called with `.$!` which, unlike
`.$`, does not return a result, and therefore does not alter `ABC.js`.

### `ABC.ap`

In the above log you also see references to `ABC.ap`. These are created when a
Clean value is shared with the JavaScript world. Most commonly this is done
with `jsWrapFun` and `jsWrapFunWithResult`. Since these are Clean functions,
there is no way to inspect their value, although by looking at the underlying
Clean code and the thing they are assigned to (`initDOMEl`, `beforeRemove`,
etc.) one can usually figure out quite easily where the Clean value is created.

## Stack traces

Generally, stack traces in a functional language are often less helpful than in
imperative or object-oriented languages, due to lazy evaluation. A stack trace
in the WebAssembly interpreter is even less useful, because you get raw code
addresses instead of function names:

```
halt 
   {0} -697 
   {1} 182779 
   {2} 287978 
   {3} 292134 
   {4} 818 
   {5} 180888 
   {6} 181364 
   {7} 181259 
   {8} -39 
   {9} 5837656.25 
   {10} 24591696.5 
   {11} 4256775 
   {12} 375410174 
   {13} 380894903.625 
   {14} 280432463.375 
   {15} 24157767.5 
   {16} 83901512.375 
   {17} 370228560.875 
   {18} 24593895.625 
   {19} 16170311.625 
   {20} 5837656.25 
```

However, there is a way to recover the function names using these numbers. For
this, you need to build a version of the C interpreter:

```bash
git clone https://gitlab.com/clean-and-itasks/abc-interpreter
cd abc-interpreter/src
make interpret
```

You can then get a program listing (somewhat like `objdump -d`) using the
bytecode of the program:

```bash
path-to-abc-interpreter/src/interpret -l -R test.bc | less
```

The numbers in the stack trace refer to addresses in the code section, which is
listed first. For example, if I look up `287978` (the address in frame 2), I
see:

```
e_StdMisc_nundef
287974  push_node0 <292380> _cycle_in_spine
287976  jsr <287979> e_StdMisc_sundef
287978  fill_a01_pop_rtn
e_StdMisc_sundef
287979  buildAC "Run-time error! Program evaluated undefined value?!"
287981  print_string
287982  halt
287983  pop_a_jmp 1 <287997> e_StdMisc_nabort+40
```

Which is correct, since the program I used to generate the stack trace used
`undef` from `StdMisc`.

A couple of notes:

- The stack trace does not stop when the root of the stack is found, so when
  the stack trace is short a part of it does not make sense (in the above case,
  you see that from frame 9 onwards there are very high and rational addresses,
  so these are probably not part of the actual trace).
- Negative addresses refer to a small bit of built-in code, defined in
  `src/bcprelink.c` in the repository of the ABC interpreter. For instance,
  address `-39` (frame 8) refers to index `697-39=658` of the
  `prelinker_preamble` array defined there, which is the ABC instruction
  `instruction 0` and is used to return to JavaScript after performing an
  evaluation in Clean (this is another hint that the real stack trace finished
  after frame 8).
- Non-integer addresses suggest bugs in the ABC interpreter. Please inform the
  maintainer.
- If you want the program listing given by `interpret -l -R` to be more like
  the ABC code you find in the (optimised) ABC code files generated by the
  compiler, you should disable `StripByteCode` in the project file. This
  prevents `cpm` from stripping unused code from the bytecode.
- It is in principle possible to automate the process of looking up label names
  using the `interpret` program, and to give a symbolic trace on the client.
  This has not been implemented because it is not used that often. If you find
  yourself using this a lot, please create an issue so that it can be
  implemented.

## Logging all ABC instructions evaluated

A final technique is to log all ABC instructions as they are evaluated. This
includes instructions used for the JavaScript interface, like to build strings
that are sent to JavaScript to evaluate.

**Because of the large number of ABC instructions that are evaluated, this will
in many cases crash your browser.**

To do this, you need to build your own version of `abc-interpreter.wasm`, and
use `INTERPRETERGENWASMFLAGS=-d` to enable extra debug calls. You also need to
set `ABC_DEBUG=true` in `abc-interpreter.js`.

```bash
git clone https://gitlab.com/clean-and-itasks/abc-interpreter
cd abc-interpreter
INTERPRETERGENWASMFLAGS=-d make -C src-js WebPublic
```

You can now use `src-js/WebPublic` instead of the `WebPublic` directory that
came with the ABC interpreter library in your Clean installation.

Note that for the build step you need the stand-alone SpiderMonkey JavaScript
shell from Mozilla (to avoid `make: js: Command not found`). It can be
downloaded from:
http://ftp.mozilla.org/pub/firefox/nightly/latest-mozilla-central/jsshell-linux-x86_64.zip
