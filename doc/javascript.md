# The JavaScript interface

The ABC interpreter provides an additional target platform besides the native
platforms supported by the Clean [code generator][]. Because the interpreter
can be compiled to both C and WebAssembly, it can be used to run Clean code in
a variety of environments, including JavaScript ones like web browsers and
[Node.js][]. In such environments, ABC.Interpreter.JavaScript provides a
foreign function interface to access the JavaScript world.

For a high level overview of that part of the system relevant to the
WebAssembly interpreter and the foreign function interface, see Staps, van
Groningen and Plasmeijer (2019), in particular sections 1, 2, 3.0, and 4.

The FFI in ABC.Interpreter.JavaScript is well-documented, and for usage
examples [iTasks][] is a good source (search for `withClientSideInit` in
implementation modules). The monadic layer in ABC.Interpreter.JavaScript is
used less, but can be seen in action in iTasks.Testing.Selenium (and
submodules). This file contains some extra high-level guidelines on top of the
documentation in these modules.

For debugging the JavaScript FFI, see
[the separate documentation](/doc/debugging-javascript.md).

## `JSVal` and the `jsValToInt` series of functions

A `JSVal` represents a value that can be represented in JavaScript. However, it
does not necessarily live in the JavaScript world. It can be one of two things:

- A complex Clean graph representing a value.
- A reference to the JavaScript world.

(When it is a complex Clean graph, it may in turn contain `JSVal`s, which may
again be either a Clean graph or a reference to the JavaScript world. In the
end, a `JSVal` may therefore contain any number of references to the JavaScript
world.)

When you use the FFI it is important to understand when a `JSVal` lives on the
Clean heap and when it lives in the JavaScript world. For this you can follow
the following rules of thumb:

- When a value is returned by `.?` or `.$`, it will be copied to Clean *as far
  as possible*. Basic values (strings, numbers, booleans, `null`, `undefined`)
  and arrays are copied into a complex Clean graph (of course, still within the
  abstract `JSVal` type&mdash;see the implementation of `JSVal`). However, for
  JavaScript objects (including functions, etc.), no equivalent Clean
  representation exists. These values are not copied (nor are there children),
  and a reference is created instead.
- Arguments to Clean functions are likewise copied as far as possible.
- When a value is created by a Clean function (`jsRecord`, `toJS`, `jsCall`,
  `jsGlobal`, `.#`), a complex Clean graph is created.

Both complex Clean graphs and references to the JavaScript world are fully
serialized to a string when they are passed to the JavaScript world for
evaluation (for instance, as the arguments to `.=` or `.$`).

To properly copy a value into the Clean world so that it can be used, it must
be lifted out of the `JSVal` type. The `jsValToInt` series of functions can be
used for this. These functions do not attempt to evaluate the `JSVal` in
JavaScript: no context switch is made. For this reason you can only use them on
complex Clean graphs. Consider the following examples:

```clean
# i = jsValToInt     (toJS 5)                          // ?Just 5

# s = jsValToString  (jsCall (jsGlobal "btoa") "xyz")  // ?None; no context switch is made

# (r,w) = jsCall (jsGlobal "btoa") "xyz" .? w
# s = jsValToString r                                  // ?Just "eHl6"

// But more idiomatic would be
# (r,w) = (jsGlobal "btoa" .$ "xyz") w
# s = jsValToString r                                  // ?Just "eHl6"
```

`jsIsUndefined` and `jsIsNull` work similarly.

An exception is `jsValToList`, which *does* perform context switches. This
allows it to work on any JavaScript iterable, not just arrays.

## Performance guidelines

In general it is a good idea to set `ABC_DEBUG=true` in abc-interpreter.js (in
the WebPublic directory of the ABC interpreter) when you are working with the
FFI interface. This prints the strings that are sent to JavaScript for
evaluation, and helps to identify cases where there are too many evaluations or
where the generated strings get unexpectedly large.

Below follow a number of specific optimisations that can be considered.

### Limit context switches

Every FFI call triggers an evaluation of a string in JavaScript. This requires
the JavaScript engine to run the string through a JIT compiler. This is a
simple compiler but can nevertheless cause a significant overhead; some crude
experiments at TOP Software suggested that as much as 30% of the total running
time of an interpreted Clean function may be spent in the JIT compiler. This
overhead can partially be alleviated by limiting the number of context switches
between WebAssembly and JavaScript.

Only functions on `*JSWorld` make a context switch and can modify the
JavaScript world. As such, the number of applications on `*JSWorld` should be
minimised (there are some exceptions to this rule, which will be covered
below).

For example, a pattern commonly found in the iTasks source is the following:

```clean
	# (taskId,w)   = me .# "attributes.taskId" .? w
	# (editorId,w) = me .# "attributes.editorId" .? w
	# (_,w)        = (me .# "doEditEvent" .$ (taskId, editorId, toJSON value)) w
```

There are three context switches here (twice `.?` and once `.$`). This is not
needed, because `JSVal`s living in the Clean world can be given as an argument
to `.$` as well:

```clean
	# taskId   = me .# "attributes.taskId"
	# editorId = me .# "attributes.editorId"
	# (_,w)    = (me .# "doEditEvent" .$ (taskId, editorId, toJSON value)) w
```

You can yourself check how many context switches are made. When `ABC_DEBUG` in
abc-interpreter.js (in the WebPublic directory of the ABC interpreter) is set
to `true`, debug information of the FFI will be printed to the console.

With the original code using `.?` you will see something like:

```
eval ABC.js[0].attributes.taskId
eval ABC.js[0].attributes.editorId
eval ABC.js[0]['doEditEvent']('8-2','v0-0',['Just','value'])
```

The optimized code instead shows only:

```
eval ABC.js[0]['doEditEvent'](ABC.js[0].attributes.taskId,ABC.js[0].attributes.editorId,['Just','value'])
```

Another example:

```clean
	# (value,w) = me .# "attributes.value" .? w
	# (domEl,w) = me .# "domEl" .? w
	# w         = (domEl .# "value" .= value) w
```

Assuming `value` and `domEl` are not used elsewhere, we can avoid copying them
to Clean:

```clean
	# w = (me .# "domEl.value" .= me .# "attributes.value") w
```

Observe that this will work efficiently even for large values in
`me.attributes.value`. The original code would copy the value first to Clean,
and then back, and may even cause a run-time error if the value does not fit in
the Clean heap.

Finally, in cases where many properties need to be set, like here:

```clean
	# w = (obj .# "prop1" .= 1) w
	# w = (obj .# "prop2" .= 2) w
	# w = (obj .# "prop3" .= 3) w
	# w = (obj .# "prop4" .= 4) w
```

One can use `Object.assign`, which requires only one call to the FFI:

```clean
	# w = (jsGlobal "Object.assign" .$!
		( obj
		, jsRecord
			[ "prop1" :> 1
			, "prop2" :> 2
			, "prop3" :> 3
			, "prop4" :> 4
			]
		)) w
```

### Limit copying of values

A second optimization can be made here: the return value of the `.$`
application is not used, so we can use `.$!` instead:

```clean
	# taskId   = me .# "attributes.taskId"
	# editorId = me .# "attributes.editorId"
	# w        = (me .# "doEditEvent" .$! (taskId, editorId, toJSON value)) w
```

When `.$` is used, the FFI will copy the result to Clean, because it cannot
know that the value will not be used. With `.$!`, the value is not copied.

(The difference between `.$` and `.$!` cannot be observed with
`ABC_DEBUG=true`.)

### Limit the size of strings copied to JavaScript

The FFI works by building strings representing JavaScript expressions in Clean
and evaluating them in a specific scope in the JavaScript world. Although it
will normally not make a big difference, there can be cases where the size of
the strings passed to JavaScript can be optimised. This can be observed in the
example above. The code executed through the FFI was:

```
eval ABC.js[0].attributes.taskId
eval ABC.js[0].attributes.editorId
eval ABC.js[0]['doEditEvent']('8-2','v0-0',['Just','value'])
```

By removing `.?` applications we could make this:

```
eval ABC.js[0]['doEditEvent'](ABC.js[0].attributes.taskId,ABC.js[0].attributes.editorId,['Just','value'])
```

This is an optimisation if `taskId` and `editorId` are indeed used only once.
However, when they are used very frequently, it may actually be more efficient
to just copy them to Clean with `.?` so that the strings built for further
calls to the FFI (using those values) are shorter.

### Avoid `jsEmptyObject`

`jsEmptyObject` (which performs a context switch to create an empty JavaScript
object, `{}`) may have some use cases (I'm actually doubting this), but it can
usually be avoided by using `jsRecord` instead, to save a number of context
switches. `jsRecord` also makes the code more readable.

For example:

```clean
	# (cfg,w) = jsEmptyObject w
	# w       = (cfg .# "field" .= domEl) w
	# w       = (cfg .# "format" .= "YYYY-MM-DD") w
	# w       = (cfg .# "firstDay" .= 1) w
	# w       = (cfg .# "onSelect" .= onSelectCb) w
```

Can be rewritten to:

```clean
	# cfg = jsRecord
		[ "field"    :> domEl
		, "format"   :> "YYYY-MM-DD"
		, "firstDay" :> 1
		, "onSelect" :> onSelectCb
		]
```

(The only difference is that in the updated code `cfg` lives on the Clean heap
instead of in the JavaScript world. This is usually not a problem, because
`JSVal`s are automatically serialized when they are sent to the JavaScript
world. Only when the object becomes very large it may be useful to store it on
the JavaScript side, to save space in the Clean heap.)

## References

Staps, Camil, John van Groningen and Rinus Plasmeijer, 2019.
[Lazy Interworking of Compiled and Interpreted Code for Sandboxing and Distributed Systems][ifl19],
in Jurriën Stutterheim and Wei Ngan Chin (eds.), Implementation and Application
of Functional Languages (IFL '19), September 25–27, 2019, Singapore, Singapore.
doi: [10.1145/3412932.3412941](https://doi.org/10.1145/3412932.3412941)

[code generator]: https://gitlab.science.ru.nl/clean-compiler-and-rts/code-generator
[ifl19]: https://camilstaps.nl/assets/pdf/ifl2019.pdf
[iTasks]: https://gitlab.com/clean-and-itasks/itasks-sdk
[Node.js]: https://nodejs.org/en/
