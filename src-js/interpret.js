let fs = require('fs/promises');
let path = require('path');
let process = require('process');

global.fetch = function (request) {
	if (request.indexOf('/js/')==0)
		request = path.join(__dirname, '../lib/WebPublic', request);

	return fs.readFile(request).then(buffer => {return {
		ok: true,
		arrayBuffer: () => buffer.buffer
	};});
}

const ABCInterpreter = require('../lib/WebPublic/js/abc-interpreter.js').ABCInterpreter;

var stack_size=512<<10;
var heap_size=2<<20;

function string_to_size(s) {
	var res=0;
	var i=0;

	while ('0'<=s[i] && s[i]<='9') {
		res*=10;
		res+=s.charCodeAt(i)-48;
		i++;
	}

	if (i>=s.length)
		return res;

	switch (s[i]) {
		case 'm':
		case 'M':
			res<<=10;
		case 'k':
		case 'K':
			res<<=10;
			i++;
			break;
		default:
			return -1;
	}

	if (i==s.length)
		return res;
	return -1;
}

const heapi=process.argv.lastIndexOf('-h');
if (heapi>=0)
	heap_size=string_to_size(process.argv[heapi+1]);

const stacki=process.argv.lastIndexOf('-s');
if (stacki>=0)
	stack_size=string_to_size(process.argv[stacki+1]);
stack_size*=2;

ABC=null;
ABCInterpreter.instantiate({
	bytecode_path: process.argv[process.argv.length-1],
	heap_size: heap_size,
	stack_size: stack_size,

	interpreter_imports: {
		debug_instr: function (addr, instr) {
			process.stderr.write((addr/8-ABC.code_offset)+'\t'+ABCInterpreter.instructions[instr]+'\n');
		},
		illegal_instr: function (addr, instr) {
			process.stderr.write('illegal instruction '+instr+' ('+ABCInterpreter.instructions[instr]+') at address '+(addr/8-ABC.code_offset)+'\n');
			process.exit(1);
		},
		out_of_memory: function () {
			process.stderr.write('out of memory\n');
			process.exit(1);
		},
		halt: function (pc, hp_free, hp_size) {
		},

		putchar: function (v) {
			process.stdout.write(String.fromCharCode(v));
		},
		print_int: function (high,low) {
			if (high==0 && low>=0) {
				process.stdout.write(String(low));
			} else {
				var n=BigInt(high)*2n**32n;
				if (low<0) {
					n+=2n**31n;
					low+=2**31;
				}
				n+=BigInt(low);
				process.stdout.write(String(n));
			}
		},
		print_bool: function (v) {
			process.stdout.write(v==0 ? 'False' : 'True');
		},
		print_char: function (v) {
			process.stdout.write("'"+String.fromCharCode(v)+"'");
		},
		print_real: function (v) {
			process.stdout.write(Number(0+v).toLocaleString(
				['en-US'],
				{
					useGrouping: false,
					maximumSignificantDigits: 15,
				}
			));
		},
	}
}).then(function(abc){
	ABC=abc;

	if (!abc.start) {
		process.stderr.write('program has no start address\n');
		process.exit(1);
	}
	abc.interpreter.instance.exports.set_pc(abc.start);

	const time_start=new Date().getTime();

	const r=abc.interpreter.instance.exports.interpret();
	if (r!=0)
		process.stderr.write('failed with return code'+r+'\n');

	if (process.argv.indexOf('--time') >= 0) {
		const time_end=new Date().getTime();
		const time=(time_end-time_start)/1000;
		process.stderr.write('user '+time.toLocaleString(['en-US'], {maximumFractionDigits: 2})+'\n');
	}
});
