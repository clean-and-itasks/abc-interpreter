implementation module specialized

import target

instr_halt :: !Target -> Target
instr_halt t = foldl (flip append) t
	[ "#ifdef DEBUG_CURSES"
	, "\tdebugger_graceful_end();"
	, "#endif"
	, "#ifdef LINK_CLEAN_RUNTIME"
	, "\t{"
	, "\t\tEPRINTF(\"Stack trace:\\n\");"
	, "\t\tBC_WORD *start_csp = &stack[stack_size >> 1];"
	, "\t\tchar _tmp[256];"
	, "\t\tfor (; csp>start_csp; csp--) {"
	, "\t\t\tprint_label(_tmp, 256, 1, (BC_WORD*)*csp, program, ie->heap, ie->heap_size);"
	, "\t\t\tEPRINTF(\"%s\\n\",_tmp);"
	, "\t\t}"
	, "\t}"
	, "\tinterpret_error=&e__ABC_PInterpreter__dDV__Halt;"
	, "\tEXIT(ie,1);"
	, "\tgoto eval_to_hnf_return_failure;"
	, "#else"
	, "\treturn 0;"
	, "#endif"
	]

instr_clzb :: !Target -> Target
instr_clzb t = foldl (flip append) t
	[ "#ifdef WINDOWS"
	, "\t{"
	, "\tunsigned long idx;"
	, "# if WORD_WIDTH==64"
	, "\t\tunsigned char r=_BitScanReverse64(&idx, bsp[0]);"
	, "# else"
	, "\t\t_unsigned char r=BitScanReverse(&idx, bsp[0]);"
	, "# endif"
	, "\tbsp[0]=r == 0 ? WORD_WIDTH : WORD_WIDTH-idx;"
	, "\t}"
	, "#else"
	, "\tif (bsp[0]==0){"
	, "\t\tbsp[0]=WORD_WIDTH;"
	, "\t} else {"
	, "# if WORD_WIDTH==64"
	, "\t\tbsp[0]=__builtin_clzl(bsp[0]);"
	, "# else"
	, "\t\tbsp[0]=__builtin_clz(bsp[0]);"
	, "# endif"
	, "\t}"
	, "#endif"
	]

instr_divLU :: !Target -> Target
instr_divLU t = foldl (flip append) t
	[ "{"
	, "#if defined(WINDOWS) && WORD_WIDTH==64"
	, "\tEPRINTF(\"divLU is not supported on 64-bit Windows\\n\");"
	, "#else"
	, "# if WORD_WIDTH==64"
	, "\t__int128_t num=((__int128_t)bsp[0] << 64) + bsp[1];"
	, "# else"
	, "\tint64_t num=((int64_t)bsp[0] << 32) + bsp[1];"
	, "# endif"
	, "\tbsp[1]=num%bsp[2];"
	, "\tbsp[2]=num/bsp[2];"
	, "\tbsp+=1;"
	, "\tpc+=1;"
	, "#endif"
	, "}"
	]

instr_mulUUL :: !Target -> Target
instr_mulUUL t = foldl (flip append) t
	[ "{"
	, "#if defined(WINDOWS) && WORD_WIDTH==64"
	, "\tEPRINTF(\"mulUUL is not supported on 64-bit Windows\\n\");"
	, "#else"
	, "# if WORD_WIDTH==64"
	, "\t__uint128_t res=(__uint128_t)((__uint128_t)bsp[0] * (__uint128_t)bsp[1]);"
	, "# else"
	, "\tuint64_t res=(uint64_t)bsp[0] * (uint64_t)bsp[1];"
	, "# endif"
	, "\tbsp[0]=res>>WORD_WIDTH;"
	, "\tbsp[1]=(BC_WORD)res;"
	, "\tpc+=1;"
	, "#endif"
	, "}"
	]

instr_RtoAC :: !Target -> Target
instr_RtoAC t = foldl (flip append) t
	[ "{"
	, "char r[22];"
	, "union real_and_int r_and_i={.i=bsp[0]};"
	, "int n=sprintf(r,BC_REAL_FMT,r_and_i.r+0.0);"
	, "NEED_HEAP(2+((n+IF_INT_64_OR_32(7,3))>>IF_INT_64_OR_32(3,2)));"
	, "hp[0]=(BC_WORD)ADDR__STRING_+2;"
	, "hp[1]=n;"
	, "memcpy(&hp[2],r,n);"
	, "pc+=1;"
	, "bsp+=1;"
	, "asp[1]=(BC_WORD)hp;"
	, "asp+=1;"
	, "hp+=2+((n+IF_INT_64_OR_32(7,3))>>IF_INT_64_OR_32(3,2));"
	, "}"
	]

instr_RtoAC_32 :: !Target -> Target
instr_RtoAC_32 t = foldl (flip append) t
	[ "{"
	, "char r[22];"
	, "union double_and_int d_and_i;"
	, "#if WORD_WIDTH==32"
	, "d_and_i.i=*(uint64_t*)bsp;"
	, "#else"
	, "d_and_i.i=(bsp[0] & 0xffffffff) | (bsp[1]<<32);"
	, "#endif"
	, "int n=sprintf(r,BC_REAL_FMT,d_and_i.d+0.0);"
	, "NEED_HEAP(2+((n+IF_INT_64_OR_32(7,3))>>IF_INT_64_OR_32(3,2)));"
	, "hp[0]=(BC_WORD)ADDR__STRING_+2;"
	, "hp[1]=n;"
	, "memcpy(&hp[2],r,n);"
	, "pc+=1;"
	, "bsp+=2;"
	, "asp[1]=(BC_WORD)hp;"
	, "asp+=1;"
	, "hp+=2+((n+IF_INT_64_OR_32(7,3))>>IF_INT_64_OR_32(3,2));"
	, "}"
	]

instr_load_i :: !Target -> Target
instr_load_i t = foldl (flip append) t
	[ "bsp[0]=*(BC_WORD*)(bsp[0]+pc[1]);"
	, "pc+=2;"
	]

instr_load_si16 :: !Target -> Target
instr_load_si16 t = foldl (flip append) t
	[ "bsp[0]=*(short*)(bsp[0]+pc[1]);"
	, "pc+=2;"
	]

instr_load_si32 :: !Target -> Target
instr_load_si32 t = foldl (flip append) t
	[ "bsp[0]=*(int*)(bsp[0]+pc[1]);"
	, "pc+=2;"
	]

instr_load_ui8 :: !Target -> Target
instr_load_ui8 t = foldl (flip append) t
	[ "bsp[0]=*(unsigned char*)(bsp[0]+pc[1]);"
	, "pc+=2;"
	]

instr_closeF :: !Target -> Target
instr_closeF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "bsp++;"
	, "if (f==&clean_stdinout) {"
	, "\tif (!stdio_open)"
	, "\t\tIO_error(\"fclose: file not open (stdio)\");"
	, "\tstdio_open=0;"
	, "\tbsp[0]=f->file_error==0;"
	, "} else if (f==&clean_stderr) {"
	, "\tbsp[0]=1;"
	, "} else {"
	, "\tbsp[0]=fclose(f->file_handle) ? 0 : 1;"
	, "\tfree(f);"
	, "}"
	, "}"
	]

instr_endF :: !Target -> Target
instr_endF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "FILE *h;"
	, "if (f==&clean_stdinout || f==&clean_stderr)"
	, "\tIO_error(\"FEnd: not allowed for StdIO and StdErr\");"
	, "else"
	, "\th=f->file_handle;"
	, "int c=getc(h);"
	, "if (c==EOF) {"
	, "\t*--bsp=1;"
	, "} else {"
	, "\tungetc(c,h);"
	, "\t*--bsp=0;"
	, "}"
	, "}"
	]

instr_errorF :: !Target -> Target
instr_errorF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "*--bsp=f->file_error!=0;"
	, "}"
	]

// TODO: implement for stdout and stderr on Windows
instr_flushF :: !Target -> Target
instr_flushF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "int error=0;"
	, "if (f==&clean_stdinout)"
	, "#ifdef WINDOWS"
	, "\tEPRINTF (\"flushF not implemented on Windows\\n\");"
	, "#else"
	, "\terror=fflush (stdout);"
	, "#endif"
	, "else if (f==&clean_stderr)"
	, "#ifdef WINDOWS"
	, "\tEPRINTF (\"flushF not implemented on Windows\\n\");"
	, "#else"
	, "\terror=fflush (stdin);"
	, "#endif"
	, "else"
	, "\terror=fflush (f->file_handle);"
	, "f->file_error|=error;"
	, "*--bsp=error==0;"
	, "}"
	]

instr_openF :: !Target -> Target
instr_openF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=safe_malloc(sizeof(struct file));"
	, "BC_WORD *clean_file_name=(BC_WORD*)asp[0];"
	, "asp--;"
	, "char *file_name=safe_malloc(clean_file_name[1]+1);"
	, "memcpy(file_name,&clean_file_name[2],clean_file_name[1]);"
	, "file_name[clean_file_name[1]]='\\0';"
	, "if (bsp[0]>=6)"
	, "\tIO_error(\"openF: illegal mode\");"
	, "char *mode=file_modes[bsp[0]];"
	, "if (mode==NULL)"
	, "\tIO_error(\"openF: unimplemented mode\");"
	, "f->file_handle=fopen(file_name,mode);"
	, "f->file_mode=bsp[0];"
	, "f->file_error=0;"
	, "bsp-=2;"
	, "bsp[1]=0;"
	, "if (f->file_handle==NULL) {"
	, "\tbsp[0]=0;" // not ok
	, "\tfree(f);"
	, "} else {"
	, "\tbsp[0]=1;" // ok
	, "\tbsp[2]=(BC_WORD)f;"
	, "}"
	, "free(file_name);"
	, "}"
	]

instr_positionF :: !Target -> Target
instr_positionF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "if (f==&clean_stdinout || f==&clean_stderr)"
	, "\tIO_error(\"FPosition: not allowed for StdIO and StdErr\");"
	, "else"
	, "\t*--bsp=ftell(f->file_handle);"
	, "}"
	]

instr_readFC :: !Target -> Target
instr_readFC t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "bsp-=2;"
	, "if (f==&clean_stdinout) {"
	, "\tbsp[1]=getchar();"
	, "} else if (f==&clean_stderr) {"
	, "\tIO_error(\"FReadC: can't read from StdErr\");"
	, "} else {"
	, "\tbsp[1]=getc(f->file_handle);"
	, "}"
	, "bsp[0]=bsp[1]!=EOF;"
	, "}"
	]

instr_readFI :: !Target -> Target
instr_readFI t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "bsp-=2;"
	, "if (f==&clean_stdinout) {"
	, "#ifdef LINK_CLEAN_RUNTIME"
	, "\tbsp[0]=w_get_int ((BC_WORD_S*)&bsp[1]);"
	, "#else"
	, "\tbsp[0]=fscanf(stdin,BC_WORD_S_FMT,(BC_WORD_S*)&bsp[1])==1;"
	, "#endif"
	, "} else if (f==&clean_stderr) {"
	, "\tIO_error(\"FReadI: can't read from StdErr\");"
	, "} else if (F_IS_TEXT_MODE(f->file_mode)) {"
	, "\tbsp[0]=fscanf(f->file_handle,BC_WORD_S_FMT,(BC_WORD_S*)&bsp[1])==1;"
	, "} else {"
	, "\tint i;"
	, "\tbsp[0]=1;"
	, "\tbsp[1]=0;"
	, "\tfor (int n=0; n<4; n++) {"
	, "\t\tif ((i=fgetc(f->file_handle))==EOF) { bsp[0]=0; break; }"
	, "\t\t((char*)&bsp[1])[n]=i;"
	, "\t}"
	, "#if WORD_WIDTH == 64"
	, "\tbsp[1]=*(int*)&bsp[1];"
	, "#endif"
	, "}"
	, "}"
	]

instr_readFR :: !Target -> Target
instr_readFR t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "union real_and_int r_and_i;"
	, "struct file *f=(struct file*)bsp[1];"
	, "bsp-=2;"
	, "if (f==&clean_stdinout) {"
	, "#ifdef LINK_CLEAN_RUNTIME"
	, "\tdouble d;"
	, "\tbsp[0]=w_get_real (&d);"
	, "\tr_and_i.r=(BC_REAL)d;"
	, "#else"
	, "\tbsp[0]=fscanf(stdin,BC_REAL_SCAN_FMT,&r_and_i.r)==1;"
	, "#endif"
	, "} else if (f==&clean_stderr) {"
	, "\tIO_error(\"FReadI: can't read from StdErr\");"
	, "} else if (F_IS_TEXT_MODE(f->file_mode)) {"
	, "\tbsp[0]=fscanf(f->file_handle,BC_REAL_SCAN_FMT,&r_and_i.r)==1;"
	, "} else {"
	, "\tint i;"
	, "\tbsp[0]=1;"
	, "\tunion double_and_int d_and_i={.d=0.0};"
	, "\tfor (int n=0; n<8; n++) {"
	, "\t\tif ((i=fgetc(f->file_handle))==EOF) { bsp[0]=0; break; }"
	, "\t\t((char*)&d_and_i.i)[n]=i;"
	, "\t}"
	, "\tr_and_i.r=(BC_REAL)d_and_i.d;"
	, "}"
	, "bsp[1]=r_and_i.i;"
	, "}"
	]

instr_readFR_32 :: !Target -> Target
instr_readFR_32 t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "union double_and_int d_and_i;"
	, "struct file *f=(struct file*)bsp[1];"
	, "bsp-=3;"
	, "if (f==&clean_stdinout) {"
	, "#ifdef LINK_CLEAN_RUNTIME"
	, "\tbsp[0]=w_get_real (&d_and_i.d);"
	, "#else"
	, "\tbsp[0]=fscanf(stdin,BC_DREAL_SCAN_FMT,&d_and_i.d)==1;"
	, "#endif"
	, "} else if (f==&clean_stderr) {"
	, "\tIO_error(\"FReadI: can't read from StdErr\");"
	, "} else if (F_IS_TEXT_MODE(f->file_mode)) {"
	, "\tbsp[0]=fscanf(f->file_handle,BC_DREAL_SCAN_FMT,&d_and_i.d)==1;"
	, "} else {"
	, "\tint i;"
	, "\tbsp[0]=1;"
	, "\td_and_i.d=0.0;"
	, "\tfor (int n=0; n<8; n++){"
	, "\t\tif ((i=fgetc(f->file_handle))==EOF) { bsp[0]=0; break; }"
	, "\t\t((char*)&d_and_i.i)[n]=i;"
	, "\t}"
	, "}"
	, "#if WORD_WIDTH==32"
	, "bsp[1]=d_and_i.i;"
	, "#else"
	, "bsp[1]=d_and_i.i>>32;"
	, "bsp[2]=d_and_i.i & 0xffffffff;"
	, "#endif"
	, "}"
	]

instr_readFS :: !Target -> Target
instr_readFS t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "BC_WORD len=bsp[2];"
	, "NEED_HEAP(len);"
	, "bsp[2]=bsp[1];"
	, "bsp[1]=bsp[0];"
	, "bsp++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "pc++;"
	, "*++asp=(BC_WORD)hp;"
	, "hp[0]=(BC_WORD)ADDR__STRING_+2;"
	, "if (f==&clean_stdinout) {"
	, "#if defined (WINDOWS) && defined (LINK_CLEAN_RUNTIME)"
	, "\tint n=0;"
	, "\tint c;"
	, "\tchar *s=(char*)&hp[2];"
	, "\twhile (n!=len && (c=w_get_char (),c!=-1)){"
	, "\t\t*s++=c;"
	, "\t\tn++;"
	, "\t}"
	, "\thp[1]=n;"
	, "#else"
	, "\thp[1]=fread((char*)&hp[2],1,len,stdin);"
	, "#endif"
	, "} else if (f==&clean_stderr) {"
	, "\tIO_error(\"FReadS: can't read from StdErr\");"
	, "} else {"
	, "\thp[1]=fread((char*)&hp[2],1,len,f->file_handle);"
	, "}"
	, "hp+=2+((hp[1]+7)>>3);"
	, "}"
	]

instr_readLineF :: !Target -> Target
instr_readLineF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "struct file *f=(struct file*)bsp[1];"
	, "if (f==&clean_stdinout) {"
	, "\thp[0]=(BC_WORD)ADDR__STRING_+2;"
	, "\tBC_WORD max_bytes=(heap_free-2)<<IF_INT_64_OR_32(3,2);"
	, "\tchar *dest=(char*)&hp[2];"
	, "\tif (last_readLineF_failed) {"
	, "\t\tBC_WORD *old_string=(BC_WORD*)asp[0];"
	, "\t\tmemcpy(dest,&old_string[2],old_string[1]);"
	, "\t\tdest+=old_string[1];"
	, "\t\tasp[0]=(BC_WORD)hp;"
	, "\t} else {"
	, "\t\tasp[1]=(BC_WORD)hp;"
	, "\t\tasp++;"
	, "\t}"
	, "\thp[1]=clean_get_line((char*)dest,max_bytes);"
	, "\tif (hp[1]==-1) {"
	, "\t\thp[1]=max_bytes;"
	, "\t\theap_free=0;"
	, "\t\tlast_readLineF_failed=1;"
	, "\t\tGARBAGE_COLLECT;"
	, "\t} else {"
	, "\t\tBC_WORD words_used=2+((hp[1]+IF_INT_64_OR_32(7,3))>>IF_INT_64_OR_32(3,2));"
	, "\t\thp+=words_used;"
	, "\t\theap_free-=words_used;"
	, "\t}"
	, "\tpc++;"
	, "\tlast_readLineF_failed=0;"
	, "} else if (f==&clean_stderr) {"
	, "\tIO_error(\"freadline: can't read from stderr\");"
	, "} else {"
	, "\tIO_error(\"readLineF fallthrough\");" // TODO
	, "}"
	, "}"
	]

instr_seekF :: !Target -> Target
instr_seekF t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "unsigned int seek_mode=bsp[3];"
	, "int position=bsp[2];"
	, "if (seek_mode>2)"
	, "\tIO_error(\"FSeek: invalid mode\");"
	, "struct file *f=(struct file*)bsp[1];"
	, "bsp[3]=bsp[1];"
	, "bsp[2]=bsp[0];"
	, "bsp++;"
	, "if (f==&clean_stdinout || f==&clean_stderr)"
	, "\tIO_error(\"FSeek: can't seek on StdIO and StdErr\");"
	, "else"
	, "\tbsp[0]=fseek(f->file_handle,position,seek_mode) ? 0 : 1;"
	, "f->file_error|=bsp[0]!=1;"
	, "}"
	]

instr_stderrF :: !Target -> Target
instr_stderrF t = foldl (flip append) t
	[ "CHECK_FILE_IO;"
	, "pc+=1;"
	, "bsp[-1]=(BC_WORD)&clean_stderr;"
	, "bsp[-2]=-1;"
	, "bsp-=2;"
	]

instr_stdioF :: !Target -> Target
instr_stdioF t = foldl (flip append) t
	[ "CHECK_FILE_IO;"
	, "if (stdio_open)"
	, "\tIO_error(\"stdio: already open\");"
	, "pc+=1;"
	, "stdio_open=1;"
	, "bsp[-1]=(BC_WORD)&clean_stdinout;"
	, "bsp[-2]=-1;"
	, "bsp-=2;"
	]

instr_writeFC :: !Target -> Target
instr_writeFC t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "char c=*bsp++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "if (f==&clean_stdinout)"
	, "\tPUTCHAR(c);"
	, "else if (f==&clean_stderr)"
	, "\tEPUTCHAR(c);"
	, "else"
	, "\tputc(c,f->file_handle);"
	, "}"
	]

instr_writeFI :: !Target -> Target
instr_writeFI t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "BC_WORD_S i=*bsp++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "if (f==&clean_stdinout)"
	, "\tPRINTF(BC_WORD_S_FMT,i);"
	, "else if (f==&clean_stderr)"
	, "\tEPRINTF(BC_WORD_S_FMT,i);"
	, "else if (F_IS_TEXT_MODE(f->file_mode))"
	, "\tfprintf(f->file_handle,BC_WORD_S_FMT,i);"
	, "else {"
	, "\tputc(((char*)&i)[0],f->file_handle);"
	, "\tputc(((char*)&i)[1],f->file_handle);"
	, "\tputc(((char*)&i)[2],f->file_handle);"
	, "\tputc(((char*)&i)[3],f->file_handle);"
	, "}"
	, "}"
	]

instr_writeFR :: !Target -> Target
instr_writeFR t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "union real_and_int r_and_i={.i=bsp[0]};"
	, "r_and_i.r=r_and_i.r+0.0;"
	, "bsp++;"
	, "struct file *f=(struct file*)bsp[1];"
	, "if (f==&clean_stdinout)"
	, "\tPRINTF(BC_REAL_FMT,r_and_i.r);"
	, "else if (f==&clean_stderr)"
	, "\tEPRINTF(BC_REAL_FMT,r_and_i.r);"
	, "else if (F_IS_TEXT_MODE(f->file_mode))"
	, "\tfprintf(f->file_handle,BC_REAL_FMT,r_and_i.r);"
	, "else {"
	, "#if WORD_WIDTH==32"
	, "\t/* This is the bad case: the 32-bit interpreter has 32-bit Reals, unlike"
	, "\t * native 32-bit Clean. We have to write 8 bytes to the file to have the"
	, "\t * same behaviour, so we first cast to a double. */"
	, "\tunion double_and_int d_and_i={.d=r_and_i.r};"
	, "\tuint64_t i=d_and_i.i;"
	, "#else"
	, "\tuint64_t i=r_and_i.i;"
	, "#endif"
	, "\tputc(((char*)&i)[0],f->file_handle);"
	, "\tputc(((char*)&i)[1],f->file_handle);"
	, "\tputc(((char*)&i)[2],f->file_handle);"
	, "\tputc(((char*)&i)[3],f->file_handle);"
	, "\tputc(((char*)&i)[4],f->file_handle);"
	, "\tputc(((char*)&i)[5],f->file_handle);"
	, "\tputc(((char*)&i)[6],f->file_handle);"
	, "\tputc(((char*)&i)[7],f->file_handle);"
	, "}"
	, "}"
	]

instr_writeFR_32 :: !Target -> Target
instr_writeFR_32 t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "pc++;"
	, "union double_and_int d_and_i;"
	, "#if WORD_WIDTH==32"
	, "d_and_i.i=*(BC_REAL*)bsp;"
	, "#else"
	, "d_and_i.i=(bsp[0] & 0xffffffff) | (bsp[1]<<32);"
	, "#endif"
	, "d_and_i.d=d_and_i.d+0.0;"
	, "bsp+=2;"
	, "struct file *f=(struct file*)bsp[1];"
	, "if (f==&clean_stdinout)"
	, "\tPRINTF(BC_REAL_FMT,d_and_i.d);"
	, "else if (f==&clean_stderr)"
	, "\tEPRINTF(BC_REAL_FMT,d_and_i.d);"
	, "else if (F_IS_TEXT_MODE(f->file_mode))"
	, "\tfprintf(f->file_handle,BC_REAL_FMT,d_and_i.d);"
	, "else {"
	, "\tputc(((char*)&d_and_i.i)[0],f->file_handle);"
	, "\tputc(((char*)&d_and_i.i)[1],f->file_handle);"
	, "\tputc(((char*)&d_and_i.i)[2],f->file_handle);"
	, "\tputc(((char*)&d_and_i.i)[3],f->file_handle);"
	, "\tputc(((char*)&d_and_i.i)[4],f->file_handle);"
	, "\tputc(((char*)&d_and_i.i)[5],f->file_handle);"
	, "\tputc(((char*)&d_and_i.i)[6],f->file_handle);"
	, "\tputc(((char*)&d_and_i.i)[7],f->file_handle);"
	, "}"
	, "}"
	]

instr_writeFS :: !Target -> Target
instr_writeFS t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "struct file *f=(struct file*)bsp[1];"
	, "BC_WORD *n=(BC_WORD*)asp[0];"
	, "int len=n[1];"
	, "char *s=(char*)&n[2];"
	, "pc++;"
	, "asp--;"
	, "if (f==&clean_stdinout)"
	, "\tfor (;len;len--) PUTCHAR(*s++);"
	, "else if (f==&clean_stderr)"
	, "\tfor (;len;len--) EPUTCHAR(*s++);"
	, "else"
	, "\tfwrite(s,1,len,f->file_handle);"
	, "}"
	]

instr_writeFString :: !Target -> Target
instr_writeFString t = foldl (flip append) t
	[ "{"
	, "CHECK_FILE_IO;"
	, "struct file *f=(struct file*)bsp[3];"
	, "BC_WORD *n=(BC_WORD*)asp[0];"
	, "BC_WORD start=bsp[0];"
	, "BC_WORD len=bsp[1];"
	, "if (start+len>n[1])"
	, "\tIO_error(\"Error in fwritesubstring parameters.\");"
	, "bsp+=2;"
	, "pc++;"
	, "asp--;"
	, "char *s=(char*)&n[2]+start;"
	, "if (f==&clean_stdinout)"
	, "\tfor (;len;len--) PUTCHAR(*s++);"
	, "else if (f==&clean_stderr)"
	, "\tfor (;len;len--) EPUTCHAR(*s++);"
	, "else"
	, "\tfwrite(s,1,len,f->file_handle);" // TODO
	, "}"
	]
