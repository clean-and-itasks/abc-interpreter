# Changelog

#### v1.6.1

- Fix: fully support `fill2` and `fill2_r` instructions (these were only
  partially implemented before).
- Fix: descriptor offset for ADTs updated with `fill3` instructions.

### v1.6.0

- Feature: fully support `fill3` and `fill3_r` instructions (these were only
  partially implemented before).

#### v1.5.3

- Chore: support base `3.0`.

#### v1.5.2

- Enhancement: add a version check to the WebAssembly interpreter, to fail with
  a descriptive error message for bytecode files generated with an incompatible
  version of the toolset.
- Enhancement: add descriptive error messages when bytecode parsing fails in
  `bclink`, `bcprelink`, or `interpret`.
- Enhancement: add branch hints to WebAssembly interpreter, for performance
  improvement in browsers that use them.
- Change: `bcgen -V` now outputs the ABC version number to stdout instead of
  stderr.
- Fix: prevent memory leak by making sure all memory allocated by
  `prepare_prelinked_interpretation` is freed once the constructed
  `PrelinkedInterpretationEnvironment` is not used anymore.

#### v1.5.1

- Fix: add support for the `eqR_a` ABC instruction.

### v1.5.0

- Feature: add `format_pie_symbol` to `ABC.Interpreter`, which outputs a
  human-readable string of the symbol that is located at a given program
  location.

#### v1.4.4

- Enhancement: accept base-compiler 3; provide warning for unimplemented ABC
  instructions related to arrays reserving memory up to the next power of 2:
  `eq_arrayp2size`, `empty_arrayp2`, `copy_array_next`, `add_hp_arrayp2_first`,
  `add_hp_arrayp2_next`, and `set_arraysize`.
- Removed: there is no dependency on base-compiler-itasks anymore, as the
  package is deprecated. The current version is compatible with all
  base-compiler-itasks versions, but future versions may not be.

#### v1.4.3

- Chore: allow containers ^2 and json ^3.

#### v1.4.2

- Fix: add json as an optional dependency, needed for
  ABC.Interpreter.JavaScript.

#### v1.4.1

- Fix: fix Windows library files.

### v1.4.0

- Chore: update to base 2.0.

#### v1.3.1

- Fix: fix native interworking with `{#Bool}`.
- Fix: fix WebAssembly interworking with `{#Bool}`.

### v1.3.0

- Enhancement: update array representation to the one used in base-rts 2.0.

#### v1.2.3

- Fix: fix JavaScript FFI for the smallest value in the `Int` range.

#### v1.2.2

- Fix: fix WebAssembly string deserialization for `[]`.

#### v1.2.1

- Enhancement: improve error when file I/O is not allowed in C interpreter.
- Fix: fix printing of constant `[]` as `[]` rather than `_Nil`.
- Fix: fix constant `[]` node in C interpreter when used in ABC instructions
  (e.g. `create_array_`).

### v1.2.0

- Feature: add `forEach` function to `ABC.Interpreter.JavaScript` module.

### v1.1.0

- Feature: add `perform_gc_on_idle` option to JavaScript interface to
  automatically perform garbage collection when the thread becomes idle.
- Chore: make `base-compiler-itasks` dependency optional.

## v1.0.0

First tagged release. Some recent changes:

- 2022-06-21 Add option to `addJSFromUrl` to not reload URLs if they have already been loaded before
- 2022-02-20 Add support for `select_nc` and `update_nc` ABC instructions
- 2022-01-27 Fix linker error on Windows
- 2021-11-06 Fix segmentation fault in x86 interworking when garbage collection is needed in the host
- 2021-11-06 Fix gcc-10 warnings in `writeFR`
- 2021-08-03 Fix interworking with the WebAssembly interpreter on 32-bit systems, [!176](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/176)
- 2021-07-30 Fix native interworking, which was broken due to upstream changes, [!177](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/177)
- 2021-07-10 Fixed crashes of `debug` and `interpret` with incorrect command line arguments, [!174](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/174)
- 2021-06-30 Fixed WebAssembly `ItoAC` implementation for big integers, [!171](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/171)
- 2021-06-30 Fixed copying of JavaScript `BigInt` to Clean, [!171](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/171)
- 2021-06-25 Added implementation of `repl_r_a_args_n_a` ABC instruction, [Steffen Michels](mailto:steffen@top-software.nl), [!168](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/168)
- 2021-06-18 Added `toJSONArgs` instance for `JSONNode`, [!166](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/166)
- 2021-06-18 Fixed `jsWrapFunWithResult` (since f4f4dea9, no result would be returned), [!164](https://gitlab.com/clean-and-itasks/abc-interpreter/merge_requests/164)
