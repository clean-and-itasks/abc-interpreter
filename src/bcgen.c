#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "bcgen.h"
#include "bcgen_instructions.h"
#include "bcgen_instruction_table.h"
#include "parse_abc.h"
#include "util.h"

void parse_file(FILE *file) {
	char* line = NULL;
	size_t size = 0;
	int line_number = 0;

	code_next_module();

	while (getline(&line, &size, file) > 0) {
		line_number++;
		parse_line(line, line_number);
	}
	free(line);
}

int main (int argc, char *argv[]) {
	if(argc < 2) {
		fprintf(stderr, "Error: No ABC file specified\n");
		return -1;
	}

	FILE *input_file=NULL;
	FILE *output_file = NULL;

	int i;
	for(i = 1; i < argc; i++) {
		if (!strcmp("-V",argv[i])) {
			printf("%d\n",ABC_VERSION);
			return 0;
		} else if (!strcmp("-o",argv[i]) && i<=argc-1) {
			if((output_file = fopen(argv[i + 1], "wb")) == NULL) {
				fprintf(stderr, "Error: Could not open output file: %s\n", argv[i + 1]);
				return -1;
			}
			i++;
		} else {
			if (input_file!=NULL){
				fprintf (stderr,"Error: At most one ABC file can be specified\n");
				return -1;
			} else if((input_file=fopen (argv[i],"r"))==NULL) {
				fprintf (stderr,"Error: Could not open abc file: %s\n",argv[i]);
				return -1;
			}
		}
	}

	if (!output_file) {
		fprintf(stderr, "Usage: %s ABC -o OUTPUT\n", argv[0]);
		return -1;
	}

	initialize_code();
	init_parser();
	init_instruction_table();
	load_instruction_table();
	parse_file (input_file);
	fclose (input_file);
	free_instruction_table();

	add_add_arg_labels();
	write_program(output_file);
	free_generated_program();

	return 0;
}
