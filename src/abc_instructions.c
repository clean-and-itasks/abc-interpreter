#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "abc_instructions.h"
#include "util.h"

char *instruction_name (enum instruction i)
{
	switch (i) {
#define _INSTRUCTION_NAMES
#include "abc_instructions.h"
#undef _INSTRUCTION_NAMES

		case CMAX:
#ifdef INTERPRETER
			return "eval_to_hnf_return";
#endif
			break;
	}

	EPRINTF("Unknown instruction %d\n",(int)i);
	return "";
}

const char *instruction_type (enum instruction i)
{
	switch (i)
	{
#define _INSTRUCTION_TYPES
#include "abc_instructions.h"
#undef _INSTRUCTION_TYPES

		case CMAX:
			break;
	}

	EPRINTF("Unknown instruction %d\n",(int)i);
	return "?";
}
