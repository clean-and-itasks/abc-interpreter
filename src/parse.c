#include <stdlib.h>
#include <string.h>

#include "abc_instructions.h"
#include "parse.h"
#include "util.h"

#ifdef INTERPRETER
# include "interpret.h"
#endif

#ifdef LINKER
# include "bcgen_instructions.h"
#endif

#ifdef PRELINKER
# include "bcprelink.h"
#endif

#ifdef LINK_CLEAN_RUNTIME
void preseed_symbol_matcher(struct parser *state, const char *label, void *location) {
       struct host_symbol *sym=find_host_symbol_by_name(state->program,(char*)label);
       if (sym!=NULL)
               sym->interpreter_location=location;
}
#endif

void init_parser(struct parser *state
#ifdef LINK_CLEAN_RUNTIME
		, int host_symbols_n, int host_symbols_string_length, char *host_symbols
#endif
		) {
	state->state = PS_init;
	state->program = safe_calloc(1, sizeof(struct program));

	state->ptr = 0;

	state->code_reloc_size = 0;
	state->data_reloc_size = 0;

	state->strings_size = 0;
#if defined(INTERPRETER) && WORD_WIDTH == 32
	state->read_n = 0;
	state->data_n_words = 0;
	state->words_in_strings = 0;
	state->strings = NULL;
	state->strings_ptr = 0;
	state->relocation_offset = 0;
#endif

	state->symbols_ptr = 0;

#ifdef LINK_CLEAN_RUNTIME
	state->program->host_symbols_n = host_symbols_n;
	state->program->host_symbols = safe_malloc(host_symbols_n * sizeof(struct host_symbol));
	state->program->host_symbols_strings = safe_malloc(host_symbols_string_length + host_symbols_n);

	char *symbol_strings = state->program->host_symbols_strings;
#ifdef DESCS_RELATIVE_TO_ARRAY
	BC_WORD offset=0;
#endif
	for (int i = 0; i < host_symbols_n; i++) {
		state->program->host_symbols[i].interpreter_location = (void*) -1;
		state->program->host_symbols[i].location = *(void**)host_symbols;
		host_symbols += IF_INT_64_OR_32(8,4);
		state->program->host_symbols[i].name = symbol_strings;
		for (; *host_symbols; host_symbols++)
			*symbol_strings++ = *host_symbols;
		*symbol_strings++ = '\0';
		host_symbols++;

#ifdef DESCS_RELATIVE_TO_ARRAY
		if (!strcmp(state->program->host_symbols[i].name,"__ARRAY__"))
			offset=(BC_WORD)ADDR__ARRAY_-(BC_WORD)state->program->host_symbols[i].location;
#endif
	}
#ifdef DESCS_RELATIVE_TO_ARRAY
	for (int i=0; i<host_symbols_n; i++)
		state->program->host_symbols[i].location=(void*)((BC_WORD)state->program->host_symbols[i].location+offset);
#endif

	preseed_symbol_matcher(state, "INT", (void*) ADDR_INT);
	preseed_symbol_matcher(state, "BOOL", (void*) ADDR_BOOL);
	preseed_symbol_matcher(state, "CHAR", (void*) ADDR_CHAR);
	preseed_symbol_matcher(state, "REAL", (void*) ADDR_REAL);
	preseed_symbol_matcher(state, "DREAL", (void*) ADDR_DREAL);
	preseed_symbol_matcher(state, "__ARRAY__", (void*) ADDR__ARRAY_);
	preseed_symbol_matcher(state, "__ARRAY__INT__", (void*) ADDR__ARRAY_INT_);
	preseed_symbol_matcher(state, "__ARRAY__REAL__", (void*) ADDR__ARRAY_REAL_);
	preseed_symbol_matcher(state, "__ARRAY__DREAL__", (void*) ADDR__ARRAY_DREAL_);
	preseed_symbol_matcher(state, "__ARRAY__BOOL__", (void*) ADDR__ARRAY_BOOL_);
	preseed_symbol_matcher(state, "__ARRAY__R__", (void*) ADDR__ARRAY_R_);
	preseed_symbol_matcher(state, "__STRING__", (void*) ADDR__STRING_);
	preseed_symbol_matcher(state, "__Nil", (void*) ADDR_d__Nil);
	preseed_symbol_matcher(state, "e__system__nind", (void*) &__interpreter_indirection[5]);
#endif

#ifdef LINKER
	state->code_size = 0;
	state->data_size = 0;
	state->code_offset = 0;
	state->data_offset = 0;
	state->is_main_module = 0;
#endif

#ifdef INTERPRETER
	state->program->code_symbols_matching=NULL;
	state->program->data_symbols_matching=NULL;
#endif

#ifdef DEBUG_CURSES
	state->program->nr_instructions = 0;
#endif
}

void free_parser(struct parser *state) {
#if defined(INTERPRETER) && WORD_WIDTH == 32
	if (state->strings != NULL)
		free(state->strings);
#endif
}

void next_state(struct parser *state) {
	state->ptr = 0;
#if defined(INTERPRETER) && WORD_WIDTH == 32
	state->strings_ptr = 0;
	state->relocation_offset = 0;
#endif
	state->symbols_ptr = 0;

	switch (state->state) {
		case PS_init:
			state->state = PS_code;
#ifdef LINKER
			if (state->code_size > 0)
#else
			if (state->program->code_size > 0)
#endif
				return;
		case PS_code:
			state->state = PS_strings;
			if (state->strings_size > 0)
				return;
		case PS_strings:
			state->state = PS_data;
#ifdef LINKER
			if (state->data_size > 0)
#else
			if (state->program->data_size > 0)
#endif
				return;
		case PS_data:
			state->state = PS_symbol_table;
#ifdef LINKER
			state->new_label_nodes=safe_malloc(state->program->symbol_table_size*sizeof(struct label_node*));
			state->n_labels=0;
#endif
			if (state->program->symbol_table_size > 0)
				return;
		case PS_symbol_table:
#ifdef LINKER
			merge_new_labels_and_rebalance(state->new_label_nodes,state->n_labels);
			free(state->new_label_nodes);

			if (state->is_main_module && state->program->start_symbol_id<state->program->symbol_table_size)
				code_start(state->program->symbol_table[state->program->start_symbol_id].name);
#endif
			state->state = PS_code_reloc;
			if (state->code_reloc_size > 0)
				return;
		case PS_code_reloc:
			state->state = PS_data_reloc;
			if (state->data_reloc_size > 0)
				return;
		case PS_data_reloc:
		case PS_end:
#ifdef LINKER
			state->code_offset += state->code_size;
			state->data_offset += state->data_size;
#endif
			state->state = PS_end;
			return;
	}
}

void shift_address(BC_WORD *addr) {
	*addr = ((*addr << 1) & -7) | (*addr & 3);
}

const static char* parse_program_errors[] = {
	/* 0 */ "no error",
	/* 1 */ "unexpected end of string/file; please file a bug report",
	/* 2 */ "illegal instruction; please file a bug report",
	/* 3 */ "internal state machine error; please file a bug report",
	/* 4 */ "wrong magic number; this does not look like a bytecode file",
	/* 5 */ "wrong version number; the input file may be generated with a different version of the toolset",
	NULL
};

const char *parse_program_strerror(parse_program_error_t errno) {
	return parse_program_errors[errno];
}

parse_program_error_t parse_program(struct parser *state, struct char_provider *cp) {
	char elem8;
	int16_t elem16;
	int32_t elem32;
	int64_t elem64;

	while (state->state != PS_end) {
		switch (state->state) {
			case PS_init:
			{
				uint32_t header_length,code_size;

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				if (elem32 != ABC_MAGIC_NUMBER)
					return PARSE_PROGRAM_INCORRECT_MAGIC_NUMBER;

				if (provide_chars(&header_length, sizeof(header_length), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
				if (elem32 != ABC_VERSION)
					return PARSE_PROGRAM_INCORRECT_VERSION_NUMBER;

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
#ifdef LINKER
				state->code_size = code_size = elem32;
#else
				state->program->code_size = code_size = elem32;
#endif

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
#ifdef LINKER
				add_words_in_strings(elem32);
#elif defined(INTERPRETER) && WORD_WIDTH==32
				state->words_in_strings = elem32;
#endif

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
				state->strings_size = elem32;
#if defined(INTERPRETER) && WORD_WIDTH == 32
				/* Allocate one more to prevent reading out of bounds in PS_data */
				state->strings = safe_malloc(sizeof(uint32_t*) * (elem32+1));
				state->strings[elem32] = 0;
#endif

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
#ifdef LINKER
				state->data_size = elem32;
#else
# if defined(INTERPRETER) && WORD_WIDTH==32
				state->data_n_words = elem32;
				/* Allocate extra space because strings take more words on 32-bit */
				state->program->data_size = elem32 + state->words_in_strings;
# else
				state->program->data_size = elem32;
# endif
				state->program->code = safe_malloc(sizeof(BC_WORD) * (code_size+state->program->data_size));
				state->program->data = state->program->code + code_size;
#endif

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
				state->program->symbol_table_size = elem32;
				state->program->symbol_table = safe_malloc(elem32 * sizeof(struct symbol));
				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
				state->program->symbols = safe_malloc(elem32 + state->program->symbol_table_size);

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
				state->code_reloc_size = elem32;

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
				state->data_reloc_size = elem32;

				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				header_length-=4;
				state->program->start_symbol_id = elem32;

				while (header_length >= sizeof(elem32)) {
					if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
						return PARSE_PROGRAM_UNEXPECTED_EOF;
					header_length -= sizeof(elem32);
				}
				if (header_length > 0 && provide_chars(&elem32, header_length, 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;

				next_state(state);
				break;
			}
			case PS_code: {
				if (provide_chars(&elem16, sizeof(elem16), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
#if 0
				EPRINTF(":%d\t%d\t%s %s\n", state->ptr, elem16, instruction_name(elem16), instruction_type(elem16));
#endif
#ifdef LINKER
				state->ptr++;
				store_code_elem(2, elem16);
#else
# ifdef COMPUTED_GOTOS
				state->program->code[state->ptr++] = (BC_WORD) instruction_labels[elem16];
# else
				state->program->code[state->ptr++] = elem16;
# endif
#endif
#ifdef DEBUG_CURSES
				state->program->nr_instructions++;
#endif
				const char *type = instruction_type(elem16);
				for (; *type; type++) {
#ifdef LINKER
					state->ptr++;
#endif
					switch (*type) {
						case 'n': /* Stack index */
							if (provide_chars(&elem16, sizeof(elem16), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(2, elem16);
#else
							state->program->code[state->ptr++] = elem16;
#endif
							break;
						case 'l': /* Label */
						case 'C': /* CAF label */
						case 'S': /* String label */
						case 's': /* String label */
							if (provide_chars(&elem16, sizeof(elem16), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(2, elem16);
#else
							state->program->code[state->ptr++] = elem16;
#endif
							break;
						case 'i': /* Integer */
							if (provide_chars(&elem64, sizeof(elem64), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(8, elem64);
#else
							state->program->code[state->ptr++] = elem64;
#endif
							break;
						case 'N': /* Stack index, optimised to byte width */
							if (provide_chars(&elem16, sizeof(elem16), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(2, elem16);
#else
							state->program->code[state->ptr++] = ((BC_WORD_S) elem16) * IF_INT_64_OR_32(8, 4);
#endif
							break;
						case 'I': /* Instruction */
							if (provide_chars(&elem16, sizeof(elem16), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(2, elem16);
#else
# ifdef COMPUTED_GOTOS
							state->program->code[state->ptr++] = (BC_WORD) instruction_labels[elem16];
# else
							state->program->code[state->ptr++] = elem16;
# endif
#endif
							break;
						case 'a': /* Arity */
							if (provide_chars(&elem16, sizeof(elem16), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(2, elem16);
#else
							/* Shift so that offset -1 contains the arity; this is used in the garbage collector */
							state->program->code[state->ptr++] = (BC_WORD) elem16 << IF_INT_64_OR_32(32, 0);
#endif
							break;
						case 'c': /* Char */
							if (provide_chars(&elem8, sizeof(elem8), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(1, elem8);
#else
							state->program->code[state->ptr++] = (unsigned char) elem8;
#endif
							break;
						case 'r': /* Real */
							if (provide_chars(&elem64, sizeof(elem64), 1, cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem(8, elem64);
#else
# if (WORD_WIDTH == 64)
							state->program->code[state->ptr++] = elem64;
# else
							{
								union double_and_int d_and_i={.i=elem64};
								union float_and_int f_and_i={.f=(float)d_and_i.d};
								state->program->code[state->ptr++]=(BC_WORD)f_and_i.i;
							}
# endif
#endif
							break;
						case 'R': /* double-position Real */
							if (provide_chars (&elem32,sizeof (elem32),1,cp)<0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem (4,elem32);
#else
							state->program->code[state->ptr++]=elem32;
#endif
							if (provide_chars (&elem32,sizeof (elem32),1,cp)<0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem (4,elem32);
							state->ptr++;
#else
							state->program->code[state->ptr++]=elem32;
#endif
							break;
						case 'v': /* Bit vector */
							if (provide_chars (&elem32,sizeof (elem32),1,cp) < 0)
								return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
							store_code_elem (4,elem32);
#else
							state->program->code[state->ptr++]=elem32;
#endif
							break;
						case '?':
							EPRINTF(":%d\t%d\t%s %s\n", state->ptr, elem16, instruction_name(elem16), instruction_type(elem16));
							EPRINTF("\tUnknown instruction; add to abc_instructions.c\n");
							EXIT((void*)-1,-1);
							return PARSE_PROGRAM_ILLEGAL_INSTRUCTION;
						default:
							EPRINTF("\tUnknown type character '%c' in type of %s\n",*type,instruction_name(elem16));
							EXIT((void*)-1,1);
							return PARSE_PROGRAM_ILLEGAL_INSTRUCTION;
					}
				}

#ifdef LINKER
				if (state->ptr >= state->code_size) {
#else
				if (state->ptr >= state->program->code_size) {
#endif
					state->ptr = 0;
					next_state(state);
				}
				break; }
			case PS_strings:
				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
				add_string_information(elem32 + state->data_offset);
#elif defined(INTERPRETER) && WORD_WIDTH==32
				state->strings[state->ptr] = elem32;
#endif
				if (++state->ptr >= state->strings_size)
					next_state(state);
				break;
			case PS_data:
				if (provide_chars(&elem64, sizeof(elem64), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
#ifdef LINKER
				store_data_l(elem64);
				state->ptr++;
#else
				state->program->data[state->ptr++] = elem64;
#endif
#if defined(INTERPRETER) && WORD_WIDTH==32
				/* On 64-bit, strings can be read as-is. On 32-bit, we need to
				 * read 64 bits and store them in two words. */
				if (state->strings[state->strings_ptr] == state->read_n) {
					int length = elem64;
					uint64_t bytes;
					while (length >= 8) {
						if (provide_chars(&bytes, sizeof(bytes), 1, cp) < 0)
							return PARSE_PROGRAM_UNEXPECTED_EOF;
						state->program->data[state->ptr++] = bytes & 0xffffffff;
						state->program->data[state->ptr++] = bytes >> 32;
						length -= 8;
						state->read_n++;
					}
					if (length > 0) {
						if (provide_chars(&bytes, sizeof(bytes), 1, cp) < 0)
							return PARSE_PROGRAM_UNEXPECTED_EOF;
						state->program->data[state->ptr++] = bytes & 0xffffffff;
						if (length > 4)
							state->program->data[state->ptr++] = bytes >> 32;
						state->read_n++;
					}
					state->strings_ptr++;
				}

				if (++state->read_n >= state->data_n_words)
#elif defined(LINKER)
				if (state->ptr >= state->data_size)
#else
				if (state->ptr >= state->program->data_size)
#endif
					next_state(state);
				break;
			case PS_symbol_table:
				if (provide_chars(&elem32, sizeof(elem32), 1, cp) < 0)
					return PARSE_PROGRAM_UNEXPECTED_EOF;
				state->program->symbol_table[state->ptr].offset = elem32;
#ifdef LINKER
				if (elem32 != -1)
					state->program->symbol_table[state->ptr].offset += (elem32 & 1 ? state->data_offset : state->code_offset) * 4;
#endif
				state->program->symbol_table[state->ptr].name = state->program->symbols + state->symbols_ptr;
				do {
					if (provide_chars(&elem8, sizeof(elem8), 1, cp) < 0)
						return PARSE_PROGRAM_UNEXPECTED_EOF;
					state->program->symbols[state->symbols_ptr++] = elem8;
				} while (elem8);
#if defined(INTERPRETER) || defined(PRELINKER)
# ifdef INTERPRETER
#  define INTERPRETER_OR_PRELINKER(i,u) i
# else
#  define INTERPRETER_OR_PRELINKER(i,u) (u*8)
# endif
				if (!strcmp(state->program->symbol_table[state->ptr].name, "__ARRAY__")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR__ARRAY_,PRELINKED_DESC__ARRAY_);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "__ARRAY__INT__")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR__ARRAY_INT_,PRELINKED_DESC__ARRAY_INT_);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "__ARRAY__REAL__")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR__ARRAY_REAL_,PRELINKED_DESC__ARRAY_REAL_);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "__ARRAY__DREAL__")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR__ARRAY_DREAL_,PRELINKED_DESC__ARRAY_DREAL_);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "__ARRAY__BOOL__")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR__ARRAY_BOOL_,PRELINKED_DESC__ARRAY_BOOL_);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "__ARRAY__R__")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR__ARRAY_R_,PRELINKED_DESC__ARRAY_R_);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "__STRING__")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR__STRING_,PRELINKED_DESC__STRING_);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "INT")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR_INT,PRELINKED_DESC_INT);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "BOOL")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR_BOOL,PRELINKED_DESC_BOOL);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "CHAR")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR_CHAR,PRELINKED_DESC_CHAR);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "REAL")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR_REAL,PRELINKED_DESC_REAL);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "DREAL")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR_DREAL,PRELINKED_DESC_DREAL);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "__Nil")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(ADDR_d__Nil,PRELINKED_DESC__Nil);
				} else if (!strcmp(state->program->symbol_table[state->ptr].name, "e__system__nind")) {
					state->program->symbol_table[state->ptr].offset = (BC_WORD) INTERPRETER_OR_PRELINKER(&__interpreter_indirection[5], 136);
				} else if (state->program->symbol_table[state->ptr].offset == -1) {
# ifdef DEBUG_CLEAN_LINKS
					EPRINTF("Warning: symbol '%s' is not defined.\n",state->program->symbol_table[state->ptr].name);
# endif
				} else
				if (state->program->symbol_table[state->ptr].offset & 1) /* data symbol */ {
					state->program->symbol_table[state->ptr].offset &= -2;
# if (WORD_WIDTH == 64)
					state->program->symbol_table[state->ptr].offset *= 2;
# else
					/* code[elem32] is an offset to the abstract data segment.
					 * This offset is incorrect, because strings are longer on
					 * 32-bit. Thus, we add the required offset. This is not
					 * that efficient, but it's okay since it is only for
					 * 32-bit and only during parsing. */
#  ifdef LINK_CLEAN_RUNTIME
					state->program->symbol_table[state->ptr].offset_64=(BC_WORD)state->program->data-(BC_WORD)state->program->code;
					state->program->symbol_table[state->ptr].offset_64+=(BC_WORD)state->program->data;
					state->program->symbol_table[state->ptr].offset_64+=state->program->symbol_table[state->ptr].offset<<1;
#  endif
					int temp_relocation_offset = 0;
					for (int i = 0; state->program->symbol_table[state->ptr].offset / 4 > state->strings[i] && i < state->strings_size; i++)
						temp_relocation_offset += (state->program->data[state->strings[i] + temp_relocation_offset] + 3) / 8;
					state->program->symbol_table[state->ptr].offset += temp_relocation_offset * 4;
# endif
# ifdef INTERPRETER
					state->program->symbol_table[state->ptr].offset += (BC_WORD) state->program->data;
# elif defined(PRELINKER)
					state->program->symbol_table[state->ptr].offset += ((BC_WORD)state->program->code_size)*8+sizeof(prelinker_preamble);
# endif
# ifdef LINK_CLEAN_RUNTIME
					if (state->program->symbol_table[state->ptr].name[0]) {
						int v = ((BC_WORD*)state->program->symbol_table[state->ptr].offset)[-2];
						if (v == 0) {
							/* Descriptor has a code address that is not _hnf; ignore */
						} else if (v == -1) {
							/* Descriptor has a _hnf code address */
							struct host_symbol *host_sym = find_host_symbol_by_name(state->program, state->program->symbol_table[state->ptr].name);
							if (host_sym == NULL) {
#  ifdef DEBUG_CLEAN_LINKS
								EPRINTF("Warning: symbol '%s' not present in host\n",state->program->symbol_table[state->ptr].name);
#  endif
							} else {
								((BC_WORD*)state->program->symbol_table[state->ptr].offset)[-2] = (BC_WORD) host_sym->location;
								host_sym->interpreter_location = (BC_WORD*) state->program->symbol_table[state->ptr].offset;
							}
						} else {
							/* This shouldn't happen */
							EPRINTF("Parse error: %s should have -1/0 for descriptor resolve address\n",state->program->symbol_table[state->ptr].name);
							EXIT((void*)-1,1);
							return PARSE_PROGRAM_ILLEGAL_INSTRUCTION;
						}
					}
# endif
# ifdef DEBUG_CURSES
					if (!strcmp(state->program->symbol_table[state->ptr].name, "ARRAY"))
						ARRAY = (void*) state->program->symbol_table[state->ptr].offset;
# endif
				} else /* code symbol */ {
# if (WORD_WIDTH == 64)
					state->program->symbol_table[state->ptr].offset *= 2;
# elif defined (LINK_CLEAN_RUNTIME)
					state->program->symbol_table[state->ptr].offset_64=(state->program->symbol_table[state->ptr].offset<<1)+(BC_WORD)state->program->code;
# endif
# ifdef INTERPRETER
					state->program->symbol_table[state->ptr].offset += (BC_WORD) state->program->code;
# elif defined(PRELINKER)
					state->program->symbol_table[state->ptr].offset += sizeof(prelinker_preamble);
# endif
# ifdef LINK_CLEAN_RUNTIME
					if (state->program->symbol_table[state->ptr].name[0]) {
						/* Descriptor has a _hnf code address */
						struct host_symbol *host_sym = find_host_symbol_by_name(state->program, state->program->symbol_table[state->ptr].name);
						if (host_sym != NULL) {
							host_sym->interpreter_location = (BC_WORD*) state->program->symbol_table[state->ptr].offset;
						}
					}
# endif
				}
#elif defined(LINKER)
				if (state->program->symbol_table[state->ptr].name[0] != '\0') {
					struct label *label=find_label(state->program->symbol_table[state->ptr].name);

					if (label==NULL) {
						label=safe_malloc(sizeof(struct label));
						label->label_name=safe_malloc(strlen(state->program->symbol_table[state->ptr].name)+1);
						strcpy(label->label_name,state->program->symbol_table[state->ptr].name);
						label->label_id=-1;
						label->label_offset=state->program->symbol_table[state->ptr].offset;
						label->label_module_n=0;
						label->label_specials=NULL;

						struct label_node *node=safe_malloc(sizeof(struct label_node));
						node->label_node_left=NULL;
						node->label_node_right=NULL;
						node->label_node_label_p=label;

						state->new_label_nodes[state->n_labels++]=node;
					} else if (state->program->symbol_table[state->ptr].offset!=-1){
						label->label_offset=state->program->symbol_table[state->ptr].offset;
					}

					make_label_global(label);
				}
#endif
				if (++state->ptr >= state->program->symbol_table_size)
					next_state(state);
				break;
			case PS_code_reloc:
				{
					uint32_t code_i,sym_i;
					if (provide_chars(&code_i, sizeof(code_i), 1, cp) < 0)
						return PARSE_PROGRAM_UNEXPECTED_EOF;

					if (provide_chars(&sym_i, sizeof(sym_i), 1, cp) < 0)
						return PARSE_PROGRAM_UNEXPECTED_EOF;
					struct symbol *sym = &state->program->symbol_table[sym_i];

#ifdef LINKER
					struct label *label;
					if (sym->name[0] == '\0')
						label = new_label_at_offset(sym->offset);
					else
						label = enter_label(sym->name);
					add_code_relocation(label, code_i + state->code_offset);
#else
# if (WORD_WIDTH == 64)
					shift_address(&state->program->code[code_i]);
# endif
					state->program->code[code_i] += sym->offset;
#endif
				}

				if (++state->ptr >= state->code_reloc_size)
					next_state(state);
				break;
			case PS_data_reloc:
				{
					uint32_t data_i,sym_i;
					if (provide_chars(&data_i, sizeof(data_i), 1, cp) < 0)
						return PARSE_PROGRAM_UNEXPECTED_EOF;

					if (provide_chars(&sym_i, sizeof(sym_i), 1, cp) < 0)
						return PARSE_PROGRAM_UNEXPECTED_EOF;
					struct symbol *sym = &state->program->symbol_table[sym_i];

#ifdef LINKER
					struct label *label;
					if (sym->name[0] == '\0')
						label = new_label_at_offset(sym->offset);
					else
						label = enter_label(sym->name);
					add_data_relocation(label, data_i + state->data_offset);
#else
# if (WORD_WIDTH == 64)
					shift_address(&state->program->data[data_i]);
# endif

# if defined(INTERPRETER) && WORD_WIDTH==32
					/* data_i is an offset to the abstract data segment. We need to
					 * fix it up for the extra length of strings on 32-bit. */
					while (data_i >= state->strings[state->strings_ptr]) {
						state->relocation_offset += (state->program->data[state->strings[state->strings_ptr] + state->relocation_offset] + 3) / 8;
						state->strings_ptr++;
					}
					data_i += state->relocation_offset;
# endif

					state->program->data[data_i] += sym->offset;
#endif
				}

				if (++state->ptr >= state->data_reloc_size)
					next_state(state);
				break;
			default:
				return PARSE_PROGRAM_INTERNAL_ERROR;
		}
	}

#ifdef LINK_CLEAN_RUNTIME
	sort_host_symbols_by_location(state->program);
#endif
	return PARSE_PROGRAM_NO_ERROR;
}
