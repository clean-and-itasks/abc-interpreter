#include <stdlib.h>
#include <string.h>

#include "abc_instructions.h"
#include "bcgen_instructions.h"
#include "strip.h"
#include "util.h"

#define DEBUG_STRIPPER 0
#define PRINT_STRIPPED_LABELS 0

/* This is the logic for the bytecode stripper. The goal is to find all
 * reachable parts of the code section and all needed parts of the data
 * section, given that:
 * - a given list of descriptors must be present in the output;
 * - the start symbol must be present (optional).
 *
 * There are several things to be aware of:
 * - There is no concept of different sections in the input. Therefore, much of
 *   the logic here goes into determining the boundaries of needed sections and
 *   the type of pointers to the data section.
 * - Instructions like `jmpD` require that the *order* of blocks in the output
 *   remains unchanged.
 * - We do not parse the input, so the input is in compressed forms (i.e.,
 *   instructions are 2 bytes but integer literals 8, etc.).
 *
 * The algorithm is as follows:
 *
 * 1. Preparation phase (`prepare_strip_bytecode`):
 *
 *   - Reorders labels by offset, so that we can keep the generated code in
 *     order.
 *   - Computes `code_indices`, which allows us to find the location of a
 *     particular code index in the unparsed input (needed because the unparsed
 *     input is compressed).
 *   - Computes `code_relocations` and `data_relocations`, which are needed to
 *     link relocations to labels.
 *
 * 2. Add all descriptors to keep (and optionally the start label) to the label
 *    queue.
 *
 * 3. While the label queue is non-empty:
 *
 *   a. Take a label from the queue (`mark_block`).
 *
 *   b. Add all references this block makes to the label queue. For data
 *      labels there are only few references. For code labels, we first scan
 *      back to the beginning of the block (the first instruction before the
 *      label s.t. the previous instruction ended the block (e.g. `jmp`,
 *      `rtn`). Then we walk through the block to the next block-ending
 *      instruction, adding any labels we find to the queue.
 *
 * 4. For each label that we found, write out the code/data (`copy_block`).
 */

#define LABEL_STATE_UNSEEN  0
#define LABEL_STATE_INQUEUE 1
#define LABEL_STATE_MARKED  2
#define LABEL_STATE_COPIED  3

#define LABEL_TYPE_NORMAL                 0
#define LABEL_TYPE_STRING                 1
#define LABEL_TYPE_STRING_WITH_DESCRIPTOR 2
#define LABEL_TYPE_CAF                    3

struct s_label {
	int32_t s_label_id;
	int32_t s_label_offset;
	char *s_label_name;
	struct label *s_label_generated;
	char s_label_state;
	char s_label_type;
};

struct s_relocation {
	int32_t s_relocation_offset;
	int32_t s_relocation_label_idx;
};

/* This data type is to hold a queue of the labels that are currently in the
 * `INQUEUE` state. It allows us to easily take the next label to mark. */
struct label_queue {
	uint32_t writer;
	uint32_t reader;
	uint32_t size;
	struct s_label *labels[0];
};

static uint8_t *code;
static uint32_t *code_indices;

static uint64_t *data;

static uint32_t n_labels,n_global_labels;
static struct s_label *labels;

static uint32_t code_reloc_size,data_reloc_size;
static struct s_relocation *code_relocations;
static struct s_relocation *data_relocations;

static struct program *pgrm;

static struct label_queue *label_queue;

static int export_all_labels,include_symbol_table;

/* Checks whether a label has to be exported in the generated code. */
static int export_label (const char *label)
{
	if (export_all_labels)
		return 1;
	else if (!strcmp (label,"__ARRAY__"))
		return 1;
	else if (!strcmp (label,"__ARRAY__INT__"))
		return 1;
	else if (!strcmp (label,"__ARRAY__REAL__"))
		return 1;
	else if (!strcmp (label,"__ARRAY__DREAL__"))
		return 1;
	else if (!strcmp (label,"__ARRAY__BOOL__"))
		return 1;
	else if (!strcmp (label,"__ARRAY__R__"))
		return 1;
	else if (!strcmp (label,"__STRING__"))
		return 1;
	else if (!strcmp (label,"INT"))
		return 1;
	else if (!strcmp (label,"BOOL"))
		return 1;
	else if (!strcmp (label,"CHAR"))
		return 1;
	else if (!strcmp (label,"REAL"))
		return 1;
	else if (!strcmp (label,"DREAL"))
		return 1;
	else if (!include_symbol_table)
		return 0;
	else if (!label[0])
		return 0;
	else if (!strncmp (label,"_internal",9))
		return 0;
	else
		return 1;
}

/* Initializes the `label_queue`. */
static void init_label_queue (void)
{
	label_queue=safe_malloc (sizeof (struct label_queue)+2*sizeof (struct s_label*));
	label_queue->writer=0;
	label_queue->reader=0;
	label_queue->size=2;
}

static void add_label_to_queue (struct s_label *label)
{
	if (label->s_label_state!=LABEL_STATE_UNSEEN)
		return;

#if DEBUG_STRIPPER
	EPRINTF ("add to queue: (%d,%s)\n",label->s_label_offset,label->s_label_name);
#endif

	label->s_label_state=LABEL_STATE_INQUEUE;

	if (label->s_label_name[0]!='\0'){
		label->s_label_generated=enter_label (label->s_label_name);
		if (export_label (label->s_label_name))
			make_label_global (label->s_label_generated);
	} else {
		label->s_label_generated=new_label_at_offset (label->s_label_offset);
	}
	label->s_label_generated->label_offset=-1;

	label_queue->labels[label_queue->writer++]=label;
	label_queue->writer%=label_queue->size;

	if (label_queue->writer==label_queue->reader){
		label_queue->writer=label_queue->size;
		label_queue->size<<=1;
		label_queue->reader=0;
		label_queue=safe_realloc (label_queue,sizeof (struct label_queue)+label_queue->size*sizeof (struct s_label*));
	}
}

static struct s_label *next_label_from_queue (void)
{
	if (label_queue->reader==label_queue->writer)
		return NULL;

	struct s_label *label=label_queue->labels[label_queue->reader];
	label_queue->reader++;
	label_queue->reader%=label_queue->size;
	return label;
}

/* Parses labels from the bytecode and stores them in `labels`. The
 * array is still sorted by name (with empty names at the end) here. */
static char *parse_labels (char *bytecode,int n_labels)
{
	int label_id;

	labels=safe_malloc (sizeof (struct s_label)*n_labels);

	for (label_id=0; label_id<n_labels; label_id++){
		labels[label_id].s_label_id=label_id;
		labels[label_id].s_label_offset=*(uint32_t*)bytecode;
		bytecode+=4;
		labels[label_id].s_label_name=bytecode;
		labels[label_id].s_label_generated=NULL;
		labels[label_id].s_label_state=LABEL_STATE_UNSEEN;
		labels[label_id].s_label_type=LABEL_TYPE_NORMAL;

		if (bytecode[0])
			n_global_labels=label_id;

		for (; *bytecode; bytecode++);
		bytecode++;
	}

	return bytecode;
}

/* Helper function for `partition_labels`.
 * To swap labels, we only need to swap id, offset, and name, since state and
 * type are always the same at this point. */
static inline void swap_labels (int32_t i,int32_t j)
{
	int32_t tempa;
	char *tempb;

	tempa=labels[i].s_label_id;
	labels[i].s_label_id=labels[j].s_label_id;
	labels[j].s_label_id=tempa;

	tempa=labels[i].s_label_offset;
	labels[i].s_label_offset=labels[j].s_label_offset;
	labels[j].s_label_offset=tempa;

	tempb=labels[i].s_label_name;
	labels[i].s_label_name=labels[j].s_label_name;
	labels[j].s_label_name=tempb;
}

/* Helper function for `sort_labels`. */
static inline int32_t partition_labels (int32_t lo,int32_t hi)
{
	int32_t mid=(lo+hi)>>1;
	int32_t i=lo;
	int32_t j;
	int32_t pivot;

	/* https://en.wikipedia.org/wiki/Quicksort#Choice_of_pivot */
	if (labels[mid].s_label_offset<labels[lo].s_label_offset)
		swap_labels (lo,mid);
	if (labels[hi].s_label_offset<labels[lo].s_label_offset)
		swap_labels (lo,hi);
	if (labels[mid].s_label_offset<labels[hi].s_label_offset)
		swap_labels (hi,mid);
	pivot=labels[hi].s_label_offset;

	for (j=lo; j<=hi; j++){
		if (labels[j].s_label_offset<pivot){
			swap_labels (i,j);
			i++;
		}
	}

	swap_labels (i,hi);

	return i;
}

/* Sorts labels by offset instead of name. */
static void sort_labels (int32_t lo,int32_t hi)
{
	if (lo>=hi)
		return;
	int32_t p=partition_labels (lo,hi);
	sort_labels (lo,p-1);
	sort_labels (p+1,hi);
}

/* For relocations we need to be able to found the label they are pointing to.
 * Since `labels` is sorted by offset instead of name, the indices in
 * the bytecode are incorrect. We here update the indices the relocations point
 * to to use the indices of the `labels` array. */
static void set_label_indices_in_relocations (void)
{
	uint32_t i;
	uint32_t *labels_by_idx=safe_malloc (sizeof (uint32_t)*n_labels);

	for (i=0; i<n_labels; i++)
		labels_by_idx[labels[i].s_label_id]=i;

	for (i=0; i<code_reloc_size; i++)
		code_relocations[i].s_relocation_label_idx=labels_by_idx[code_relocations[i].s_relocation_label_idx];

	for (i=0; i<data_reloc_size; i++)
		data_relocations[i].s_relocation_label_idx=labels_by_idx[data_relocations[i].s_relocation_label_idx];

	free (labels_by_idx);
}

/* Finds a label given a name. Use only *before* calling `sort_labels`. */
#ifdef LINK_CLEAN_RUNTIME
static int find_label_by_name (const char *name)
{
	int l=0;
	int r=n_global_labels-1;
	while (l<=r) {
		int i=(l+r)/2;
		struct s_label *label=&labels[i];
		int c=strcmp (label->s_label_name,name);
		if (c<0)
			l=i+1;
		else if (c>0)
			r=i-1;
		else
			return label->s_label_id;
	}
	EPRINTF ("error in find_label_by_name (%s)\n",name);
	EXIT (NULL,1);
	return -1;
}
#endif

/* Finds the last label that has an offset less than the parameter. */
static int find_last_label_before_offset (int32_t offset)
{
	int l=0;
	int r=n_labels-1;
	while (l<=r){
		int i=(l+r)>>1;
		if (labels[i].s_label_offset>offset)
			r=i-1;
		else if (labels[i].s_label_offset<offset)
			l=i+1;
		else
			break;
	}
	while (l>=0 && labels[l].s_label_offset>=offset)
		l--;
	return l;
}

/* Fills `code_indices`; returns the end of the code section. */
static uint8_t *find_code_indices (uint32_t code_size)
{
	code_indices=safe_malloc (sizeof (uint32_t)*code_size);

	int ci=0;
	int i=0;
	while (ci<code_size){
		uint16_t instr=*(uint16_t*)&code[i];
		code_indices[ci++]=i;
		i+=2;
		const char *type=instruction_type (instr);
		for (; *type; type++){
			code_indices[ci++]=-1;
			switch (*type){
				case 'n': /* Stack index */
				case 'l': /* Label */
				case 'N': /* Stack index, optimised to byte width */
				case 'I': /* Instruction */
				case 'a': /* Arity */
				case 'S': /* String label */
				case 's': /* String label */
				case 'C': /* CAF label */
					i+=2;
					break;
				case 'i': /* Int */
				case 'r': /* Real */
					i+=8;
					break;
				case 'c': /* Char */
					i+=1;
					break;
				case 'R': /* Double-position Real */
					i+=4;
					code_indices[ci++]=-1;
					i+=4;
					break;
				case 'v': /* Bit vector */
					i+=4;
					break;
				default:
					EPRINTF ("error in find_code_indices\n");
					EXIT (NULL,-1);
			}
		}
	}

	return &code[i];
}

/* Check whether an instruction ends a basic block. */
static int instruction_ends_block (int16_t instr)
{
	switch (instr){
		case Cjmp:
		case Cjmp_ap1:
		case Cjmp_ap2:
		case Cjmp_ap3:
		case Cjmp_ap4:
		case Cjmp_ap5:
		case Cjmp_ap6:
		case Cjmp_ap7:
		case Cjmp_ap8:
		case Cjmp_ap9:
		case Cjmp_ap10:
		case Cjmp_ap11:
		case Cjmp_ap12:
		case Cjmp_ap13:
		case Cjmp_ap14:
		case Cjmp_ap15:
		case Cjmp_ap16:
		case Cjmp_ap17:
		case Cjmp_ap18:
		case Cjmp_ap19:
		case Cjmp_ap20:
		case Cjmp_ap21:
		case Cjmp_ap22:
		case Cjmp_ap23:
		case Cjmp_ap24:
		case Cjmp_ap25:
		case Cjmp_ap26:
		case Cjmp_ap27:
		case Cjmp_ap28:
		case Cjmp_ap29:
		case Cjmp_ap30:
		case Cjmp_ap31:
		case Cjmp_ap32:
		case Cjmp_eval:
		case Cjmp_eval_upd:
		case Cjmp_i:
		case Cjmp_i0:
		case Cjmp_i1:
		case Cjmp_i2:
		case Cjmp_i3:
		case Cpop_a_jmp:
		case Cpop_b_jmp:
		case Cput_a_jmp:
		case Cput_b_jmp:

		/* Cbuild_node_rtn and Cbuild_node2_rtn are not included, because they
		 * are used in add_arg blocks */
		case Cfill_a01_pop_rtn:
		case Cpop_a_rtn:
		case Cpop_ab_rtn:
		case Cpop_b_rtn:
		case Crtn:

		case Cadd_arg0:
		case Cadd_arg1:
		case Cadd_arg2:
		case Cadd_arg3:
		case Cadd_arg4:
		case Cadd_arg5:
		case Cadd_arg6:
		case Cadd_arg7:
		case Cadd_arg8:
		case Cadd_arg9:
		case Cadd_arg10:
		case Cadd_arg11:
		case Cadd_arg12:
		case Cadd_arg13:
		case Cadd_arg14:
		case Cadd_arg15:
		case Cadd_arg16:
		case Cadd_arg17:
		case Cadd_arg18:
		case Cadd_arg19:
		case Cadd_arg20:
		case Cadd_arg21:
		case Cadd_arg22:
		case Cadd_arg23:
		case Cadd_arg24:
		case Cadd_arg25:
		case Cadd_arg26:
		case Cadd_arg27:
		case Cadd_arg28:
		case Cadd_arg29:
		case Cadd_arg30:
		case Cadd_arg31:

		case Chalt:

			return 1;

		default:
			return 0;
	}
}

/* Find a relocation for a particular code index. */
static struct s_relocation *find_relocation_by_offset
		(struct s_relocation *relocs,uint32_t n_relocs,int32_t offset)
{
	int l=0;
	int r=n_relocs-1;
	while (l<=r){
		int i=(l+r)>>1;
		if (relocs[i].s_relocation_offset<offset)
			l=i+1;
		else if (relocs[i].s_relocation_offset>offset)
			r=i-1;
		else
			return &relocs[i];
	}
	return NULL;
}

/* Mark a block and add dependent labels to the queue. */
static void mark_label (struct s_label *label)
{
	struct s_relocation *reloc;

	if (label->s_label_state!=LABEL_STATE_INQUEUE || label->s_label_offset<0)
		return;

#if DEBUG_STRIPPER
	EPRINTF ("mark: (%d,%s)\n",label->s_label_offset,label->s_label_name);
#endif

	label->s_label_state=LABEL_STATE_MARKED;

	if (label->s_label_offset & 1){ /* Data label */
		uint64_t *block=&data[(label->s_label_offset-1)>>2];
		uint32_t arity=block[0];

		switch (label->s_label_type){
			case LABEL_TYPE_NORMAL:
				if ((arity & 0xffff) > 256){ /* record */
					uint64_t *type_string=&block[2];

					int n_desc_labels=0;
					for (char *ts=(char*)type_string; *ts; ts++)
						if (*ts=='{')
							n_desc_labels++;
					uint64_t *desc_labels=&block[2+(type_string[-1]+sizeof (uint64_t)-1)/sizeof (uint64_t)];
					for (; n_desc_labels; n_desc_labels--){
						reloc=find_relocation_by_offset (data_relocations,data_reloc_size,desc_labels-data);
						add_label_to_queue (&labels[reloc->s_relocation_label_idx]);
						desc_labels++;
					}
				} else if ((arity & 0xffff) > 0 && (arity>>16)==0){ /* string */
				} else { /* no record */
					arity>>=3+16;

					if (arity==0){ /* desc0, descn or string */
						if (block[0]==0 || block[0]==1 || (block[0]>>16)>0){ /* desc0 or descn */
							reloc=find_relocation_by_offset (data_relocations,data_reloc_size,&block[-1]-data);
							add_label_to_queue (&labels[reloc->s_relocation_label_idx]);
						}
					} else { /* descs or normal descriptor */
						reloc=find_relocation_by_offset (data_relocations,data_reloc_size,&block[-1]-data);
						add_label_to_queue (&labels[reloc->s_relocation_label_idx]);

						if (arity==1 && (block[1]>>2) > 0){ /* descs */
						} else {
							for (int i=0; i<arity; i++){
								reloc=find_relocation_by_offset (data_relocations,data_reloc_size,&block[i*2+1]-data);
								if (reloc==NULL && i==0) /* descn */
									break;
								add_label_to_queue (&labels[reloc->s_relocation_label_idx]);
							}
						}
					}
				}
				break;
			case LABEL_TYPE_STRING:
				break;
			case LABEL_TYPE_STRING_WITH_DESCRIPTOR:
				reloc=find_relocation_by_offset (data_relocations,data_reloc_size,block-data);
				add_label_to_queue (&labels[reloc->s_relocation_label_idx]);
				break;
			case LABEL_TYPE_CAF:
				break;
		}
	} else { /* Code label */
		int ci=label->s_label_offset>>2;

		/* since the label may point to the middle of a block, first find the beginning */
		do {
			do { ci--; } while (ci>=0 && code_indices[ci]==-1);
		} while (ci>0 && !instruction_ends_block (*(int16_t*)&code[code_indices[ci]]));
		if (ci>0)
			do { ci++; } while (code_indices[ci]==-1);
		else
			ci=0;

		uint8_t *code_block=&code[code_indices[ci]];
		int label_idx=find_last_label_before_offset (ci<<2);

#if DEBUG_STRIPPER
		EPRINTF ("\tstarting at %d\n",ci);
#endif

		while (1){
			int32_t current_offset=ci<<2;
			while (labels[label_idx].s_label_offset<current_offset)
				label_idx++;
			while (labels[label_idx].s_label_offset==current_offset){
				labels[label_idx].s_label_state=LABEL_STATE_MARKED;

#if DEBUG_STRIPPER
				EPRINTF ("\tcreate label for (%d,%s)\n",labels[label_idx].s_label_offset,labels[label_idx].s_label_name);
#endif

				/* We still have to create a new label, because it may be used by relocations */
				if (labels[label_idx].s_label_name[0]!='\0'){
					labels[label_idx].s_label_generated=enter_label (labels[label_idx].s_label_name);
					if (export_label (labels[label_idx].s_label_name))
						make_label_global (labels[label_idx].s_label_generated);
				} else {
					labels[label_idx].s_label_generated=new_label_at_offset (labels[label_idx].s_label_offset);
				}
				labels[label_idx].s_label_generated->label_offset=-1;

				label_idx++;
			}

			int16_t instr=*(int16_t*)code_block;
			code_block+=2;
			const char *type=instruction_type (instr);
			ci++;
			for (; *type; type++){
				switch (*type){
					case 'n': /* Stack index */
					case 'N': /* Stack index, optimised to byte width */
					case 'I': /* Instruction */
					case 'a': /* Arity */
						code_block+=2;
						break;
					case 'l': /* Label */
						reloc=find_relocation_by_offset (code_relocations,code_reloc_size,ci);
						/* Only labels in .n/.nu directives (i.e., CA_data_*)
						 * may be NULL; otherwise, throw an error. */
						if (reloc==NULL && instr<CA_data_IIIla){
							EPRINTF ("Error: label for %s was NULL\n",instruction_name (instr));
							EXIT (NULL,1);
						}
						if (reloc!=NULL)
							add_label_to_queue (&labels[reloc->s_relocation_label_idx]);
						code_block+=2;
						break;
					case 'i': /* Int */
					case 'r': /* Real */
						code_block+=8;
						break;
					case 'c': /* Char */
						code_block+=1;
						break;
					case 'S': /* String with __STRING__ label */
					case 's': /* Plain string label */
						reloc=find_relocation_by_offset (code_relocations,code_reloc_size,ci);
						add_label_to_queue (&labels[reloc->s_relocation_label_idx]);
						if (*type=='S')
							labels[reloc->s_relocation_label_idx].s_label_type=LABEL_TYPE_STRING_WITH_DESCRIPTOR;
						else
							labels[reloc->s_relocation_label_idx].s_label_type=LABEL_TYPE_STRING;
						code_block+=2;
						break;
					case 'R': /* Double-position Real */
						code_block+=8;
						ci++;
						break;
					case 'C': /* CAF label */
						reloc=find_relocation_by_offset (code_relocations,code_reloc_size,ci);
						add_label_to_queue (&labels[reloc->s_relocation_label_idx]);
						labels[reloc->s_relocation_label_idx].s_label_type=LABEL_TYPE_CAF;
						code_block+=2;
						break;
					case 'v': /* Bit vector */
						code_block+=4;
						break;
					default:
						EPRINTF ("error in mark_block\n");
						EXIT (NULL,-1);
				}
				ci++;
			}

			if (instruction_ends_block (instr))
				break;
		}
	}
}

/* Copy a block of a label to the generated code. */
static void copy_block (struct s_label *label)
{
	struct s_relocation *reloc;

	if (label->s_label_state!=LABEL_STATE_MARKED || label->s_label_offset<0)
		return;

#if DEBUG_STRIPPER
	EPRINTF ("copy: (%d,%s)\n",label->s_label_offset,label->s_label_name);
#endif

	label->s_label_state=LABEL_STATE_COPIED;

	if (label->s_label_offset & 1){ /* data */
		uint64_t *block=&data[(label->s_label_offset-1)>>2];
		uint32_t arity=block[0];

		switch (label->s_label_type){
			case LABEL_TYPE_NORMAL:
				if ((arity & 0xffff) > 256){ /* record */
					uint64_t *type_string=&block[2];

					store_data_l (block[-2]);
					store_data_l (block[-1]);
					label->s_label_generated->label_offset=(pgrm->data_size<<2)+1;
					store_data_l (block[0]);

					store_string ((char*)type_string,type_string[-1]-1,1);

					int n_desc_labels=0;
					for (char *ts=(char*)type_string; *ts; ts++)
						if (*ts=='{')
							n_desc_labels++;
					uint64_t *desc_labels=&block[2+(type_string[-1]+sizeof (uint64_t)-1)/sizeof (uint64_t)];
					for (; n_desc_labels; n_desc_labels--){
						reloc=find_relocation_by_offset (data_relocations,data_reloc_size,desc_labels-data);
						add_data_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->data_size);
						store_data_l (*desc_labels++);
					}

					uint64_t *name_string=desc_labels+1;
					store_string ((char*)name_string,name_string[-1],0);
				} else if ((arity & 0xffff) > 0 && (arity>>16)==0){ /* string */
					label->s_label_generated->label_offset=(pgrm->data_size<<2)+1;
					store_string ((char*)&block[1],block[0],0);
				} else { /* no record */
					arity>>=3+16;

					if (arity==0){ /* desc0, descn or string */
						if (block[0]==0 || block[0]==1 || (block[0]>>16)>0){ /* desc0 or descn */
							store_data_l (block[-3]);
							store_data_l (block[-2]);
							reloc=find_relocation_by_offset (data_relocations,data_reloc_size,&block[-1]-data);
							add_data_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->data_size);
							store_data_l (block[-1]);
							label->s_label_generated->label_offset=(pgrm->data_size<<2)+1;
							store_data_l (block[0]);
							store_data_l (block[1]);
							if (block[2]!=0){ /* descn */
								store_string ((char*)&block[3],block[2],0);
							} else { /* desc0 */
								store_data_l (block[2]);
								store_string ((char*)&block[4],block[3],0);
							}
						} else { /* string */
							store_string ((char*)&block[1],block[0],0);
						}
					} else { /* descs or normal descriptor */
						store_data_l (block[-2]);
						reloc=find_relocation_by_offset (data_relocations,data_reloc_size,&block[-1]-data);
						add_data_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->data_size);
						store_data_l (block[-1]);
						label->s_label_generated->label_offset=(pgrm->data_size<<2)+1;

						int include_last_words=1;

						if (arity==1 && (block[1]>>2) > 0){ /* descs */
							store_data_l (block[0]);
							store_data_l (block[1]);
						} else {
							for (int i=0; i<arity; i++){
								store_data_l (block[i*2]);
								reloc=find_relocation_by_offset (data_relocations,data_reloc_size,&block[i*2+1]-data);
								if (reloc==NULL && i==0){ /* descn */
									store_data_l (block[1]);
									store_string ((char*)&block[3],block[2],0);
									include_last_words=0;
									break;
								}
								add_data_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->data_size);
								store_data_l (block[i*2+1]);
							}
						}

						if (include_last_words){
							store_data_l (block[2*arity]);
							store_data_l (block[2*arity+1]);
							store_data_l (block[2*arity+2]);
							store_string ((char*)&block[2*arity+4],block[2*arity+3],0);
						}
					}
				}
				break;
			case LABEL_TYPE_STRING:
				label->s_label_generated->label_offset=(pgrm->data_size<<2)+1;
				store_string ((char*)&block[1],block[0],0);
				break;
			case LABEL_TYPE_STRING_WITH_DESCRIPTOR:
				label->s_label_generated->label_offset=(pgrm->data_size<<2)+1;
				reloc=find_relocation_by_offset (data_relocations,data_reloc_size,block-data);
				add_data_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->data_size);
				store_data_l (block[0]);
				store_string ((char*)&block[2],block[1],0);
				break;
			case LABEL_TYPE_CAF:
				{
					int s=block[-1];
					if (s==0){
						s=block[-2];
						store_data_l (block[-2]);
					}
					store_data_l (block[-1]);
					label->s_label_generated->label_offset=(pgrm->data_size<<2)+1;
					for (; s; s--)
						store_data_l (0);
				}
				break;
		}
	} else { /* code */
		int ci=label->s_label_offset>>2;

		/* since the label may point to the middle of a block, first find the beginning */
		do {
			do { ci--; } while (ci>=0 && code_indices[ci]==-1);
		} while (ci>0 && !instruction_ends_block (*(int16_t*)&code[code_indices[ci]]));
		if (ci>0)
			do { ci++; } while (code_indices[ci]==-1);
		else
			ci=0;

		uint8_t *code_block=&code[code_indices[ci]];
		int label_idx=find_last_label_before_offset (ci<<2);

#if DEBUG_STRIPPER
		EPRINTF ("\tstarting at %d\n",ci);
#endif

		while (1){
			int32_t current_offset=ci<<2;
			while (labels[label_idx].s_label_offset<current_offset)
				label_idx++;
			while (labels[label_idx].s_label_offset==current_offset){
				labels[label_idx].s_label_state=LABEL_STATE_COPIED;
				labels[label_idx].s_label_generated->label_offset=pgrm->code_size<<2;
				label_idx++;
			}

			int16_t instr=*(int16_t*)code_block;
			store_code_elem (2,instr);
			code_block+=2;
			const char *type=instruction_type (instr);
			ci++;
			for (; *type; type++){
				switch (*type){
					case 'l': /* Label */
						reloc=find_relocation_by_offset (code_relocations,code_reloc_size,ci);
						/* Only labels in .n/.nu directives (i.e., CA_data_*)
						 * may be NULL; otherwise, throw an error. */
						if (instr<CA_data_IIIla && reloc==NULL){
							EPRINTF ("Error: label for %s was NULL\n",instruction_name (instr));
							EXIT (NULL,1);
						}
						if (reloc!=NULL)
							add_code_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->code_size);
						store_code_elem (2,*(uint32_t*)code_block);
						code_block+=2;
						break;
					case 'n': /* Stack index */
					case 'N': /* Stack index, optimised to byte width */
					case 'I': /* Instruction */
					case 'a': /* Arity */
						store_code_elem (2,*(uint16_t*)code_block);
						code_block+=2;
						break;
					case 'i': /* Int */
					case 'r': /* Real */
						store_code_elem (8,*(uint64_t*)code_block);
						code_block+=8;
						break;
					case 'c': /* Char */
						store_code_elem (1,*(uint8_t*)code_block);
						code_block+=1;
						break;
					case 'S': /* String with __STRING__ label */
					case 's': /* Plain string label */
						reloc=find_relocation_by_offset (code_relocations,code_reloc_size,ci);
						add_code_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->code_size);
						store_code_elem (2,*(uint32_t*)code_block);
						code_block+=2;
						break;
					case 'R': /* Double-position Real */
						store_code_elem (4,*(uint32_t*)code_block);
						code_block+=4;
						store_code_elem (4,*(uint32_t*)code_block);
						code_block+=4;
						ci++;
						break;
					case 'C': /* CAF label */
						reloc=find_relocation_by_offset (code_relocations,code_reloc_size,ci);
						add_code_relocation (labels[reloc->s_relocation_label_idx].s_label_generated,pgrm->code_size);
						store_code_elem (2,*(uint32_t*)code_block);
						code_block+=2;
						break;
					case 'v': /* Bit vector */
						store_code_elem (4,*(uint32_t*)code_block);
						code_block+=4;
						break;
					default:
						EPRINTF ("error in copy_block\n");
						EXIT (NULL,-1);
				}
				ci++;
			}

			if (instruction_ends_block (instr))
				break;
		}
	}
}

/* Prepares for stripping by parsing the bytecode and setting up some auxiliary
 * data structures: `labels` and `code_indices`. */
#ifdef LINK_CLEAN_RUNTIME
static
#endif
void prepare_strip_bytecode (uint32_t *bytecode,int _include_symbol_table,
		int activate_start_label
#ifdef LINK_CLEAN_RUNTIME
		,struct clean_string **descriptors
#endif
		)
{
	include_symbol_table=_include_symbol_table;
	export_all_labels=0;

	if (bytecode[0]!=ABC_MAGIC_NUMBER){
		EPRINTF ("file to strip does not start with right magic number\n");
		EXIT (NULL,-1);
	}
	code=&((uint8_t*)bytecode)[bytecode[1]+8];
	if (bytecode[2]!=ABC_VERSION){
		EPRINTF ("file to strip ABC* version does not match\n");
		EXIT (NULL,-1);
	}

	uint32_t code_size=bytecode[3];
	/*uint32_t words_in_strings=bytecode[4];*/
	uint32_t strings_size=bytecode[5];
	uint32_t data_size=bytecode[6];
	n_labels=bytecode[7];
	/*uint32_t global_label_string_count=bytecode[8];*/
	code_reloc_size=bytecode[9];
	data_reloc_size=bytecode[10];
	int32_t start_label_id=bytecode[11];

	bytecode=(uint32_t*)find_code_indices (code_size);

	bytecode+=strings_size;
	data=(uint64_t*)bytecode;
	bytecode=(uint32_t*)&data[data_size];

	pgrm=initialize_code();

	bytecode=(uint32_t*)parse_labels ((char*)bytecode,n_labels);

	int32_t start_label_offset=-1;
	if (activate_start_label && start_label_id>=0)
		start_label_offset=labels[start_label_id].s_label_offset;

	code_relocations=safe_malloc (sizeof (struct s_relocation)*code_reloc_size);
	memcpy (code_relocations,bytecode,sizeof (struct s_relocation)*code_reloc_size);
	bytecode+=code_reloc_size<<1;

	data_relocations=safe_malloc (sizeof (struct s_relocation)*data_reloc_size);
	memcpy (data_relocations,bytecode,sizeof (struct s_relocation)*data_reloc_size);

#ifdef LINK_CLEAN_RUNTIME
	char *label_is_included=NULL;
	if (descriptors!=NULL){
		label_is_included=safe_calloc (1,n_labels);

		for (int i=0; i<((BC_WORD*)descriptors)[-1]; i++)
			label_is_included[find_label_by_name (descriptors[i]->cs_characters)]=1;
	}
#endif

	sort_labels (0,n_labels-1);
	set_label_indices_in_relocations();

	init_label_queue();

	if (start_label_offset>=0){
		int i=find_last_label_before_offset (start_label_offset);
		for (; i<(int)n_labels; i++){
			if (labels[i].s_label_id==start_label_id){
				add_label_to_queue (&labels[i]);
				code_start (labels[i].s_label_name);
				break;
			}
		}
	}

#ifdef LINK_CLEAN_RUNTIME
	if (label_is_included!=NULL){
		for (int i=0; i<n_labels; i++){
			if (label_is_included[labels[i].s_label_id]){
				add_label_to_queue (&labels[i]);
				make_label_global (labels[i].s_label_generated);
			}
		}
	}
#endif
}

#if PRINT_STRIPPED_LABELS
static int must_print_label (const char *l)
{
	int i;

	if (l[0]!='e' || l[1]!='_' || l[2]!='_')
		return 0;

	i=3;

	while (l[i]!='_' || l[i+1]!='_'){
		i++;
		if (!l[i])
			return 0;
	}

	return l[i+2]=='s';
}

static void print_label (const char *l)
{
	int has_module=0;

	for (l+=3; *l; l++){
		if (*l!='_'){
			fputc (*l,stderr);
			continue;
		}

		switch (*++l){
			case '_':
				if (has_module){
					fputc ('_',stderr);
				} else {
					has_module=1;
					fputc (':',stderr);
					fputc (' ',stderr);
				}
				break;
			case 'P': fputc ('.',stderr); break;
			case 'M': fputc ('*',stderr); break;
			case 'S': fputc ('-',stderr); break;
			case 'A': fputc ('+',stderr); break;
			case 'E': fputc ('=',stderr); break;
			case 'T': fputc ('~',stderr); break;
			case 'L': fputc ('<',stderr); break;
			case 'G': fputc ('>',stderr); break;
			case 'D': fputc ('/',stderr); break;
			case 'Q': fputc ('?',stderr); break;
			case 'H': fputc ('#',stderr); break;
			case 'C': fputc (':',stderr); break;
			case 'O': fputc ('|',stderr); break;
			case 'B': fputc ('`',stderr); break;
			case 'I': fputc (';',stderr); break;
			case 'N':
				switch (*++l){
					case 'D': fputc ('$',stderr); break;
					case 'C': fputc ('^',stderr); break;
					case 'T': fputc ('@',stderr); break;
					case 'A': fputc ('&',stderr); break;
					case 'P': fputc ('%',stderr); break;
					case 'S': fputc ('\'',stderr); break;
					case 'Q': fputc ('"',stderr); break;
					case 'B': fputc ('\\',stderr); break;
					case 'E': fputc ('!',stderr); break;
					default:
						fprintf (stderr,"unknown escape sequence in label: _N%c\n",*l);
						exit (1);
				}
				break;
			default:
				fprintf (stderr,"unknown escape sequence in label: _%c\n",*l);
				exit (1);
		}
	}
}
#endif

/* Finish stripping bytecode: empty the label queue and generate code. */
#ifdef LINK_CLEAN_RUNTIME
static
#endif
char *finish_strip_bytecode (uint32_t *result_size)
{
	struct s_label *label;
	int i;

	while ((label=next_label_from_queue())!=NULL)
		mark_label (label);

	for (i=0; i<n_labels; i++){
		while (labels[i].s_label_state!=LABEL_STATE_MARKED && i<n_labels){
#if PRINT_STRIPPED_LABELS
			if (labels[i].s_label_state!=LABEL_STATE_COPIED && must_print_label (labels[i].s_label_name)){
				print_label (labels[i].s_label_name);
				fputc ('\n',stderr);
			}
#endif
			i++;
		}

		if (i>=n_labels)
			break;

		copy_block (&labels[i]);
	}

	return write_program_to_string (result_size);
}

#ifdef LINK_CLEAN_RUNTIME
/* Strips bytecode for a set of descriptors. Used in the Clean library. */
void strip_bytecode (int _include_symbol_table,
		uint32_t *bytecode,struct clean_string **descriptors,
		uint32_t *result_size,char **result)
{
	prepare_strip_bytecode (bytecode,_include_symbol_table,0,descriptors);
	export_all_labels=1;
	*result=finish_strip_bytecode (result_size);
}
#endif
