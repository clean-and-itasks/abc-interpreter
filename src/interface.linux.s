# vim: ts=8:
.intel_syntax noprefix

.text

.macro save_registers
	push	ecx
	push	eax
	push	edx
.endm
.macro restore_registers
	pop	edx
	pop	eax
	pop	ecx
.endm

.macro save_host_status_via_ebp # ebp is the interpretation_environment
	mov	ebp,[ebp]   # first member of ie: host_status
	mov	[ebp],esi   # set astack pointer
	mov	[ebp+4],edi # set heap pointer
.endm
.macro restore_host_status_via_edi # edi is the interpretation_environment
	mov	edi,[edi]
	mov	esi,[edi]
	mov	edi,[edi+4]
.endm

.globl	__interpret__copy__node__asm
__interpret__copy__node__asm:
	save_registers

	# Get host_status from InterpretationEnvironment
	mov	ebp,[edx+4] # First argument of ADT constructor
	mov	ebp,[ebp+8] # Second block of finalizer
	mov	ebp,[ebp+4] # Second argument of finalizer, ptr to interpretation_environment
	push	ebp
	save_host_status_via_ebp
	mov	[ebp+8],edx

	# Parameters are in the right registers; see comment on the C function
	mov	ebp,esp
	and	esp,-16
	emms
	call	copy_interpreter_to_host
	mov	esp,ebp
__interpret__copy__node__asm_finish:
	mov	ebp,eax
	pop	edi
	restore_host_status_via_edi
	restore_registers
	cmp	ebp,0 # Redirect to host node
	je	__interpret__copy__node__asm_redirect

	mov	ecx,edi
	shl	ebp,2
	add	edi,ebp

	ret

.global	__interpret__copy__node__asm_redirect_node
__interpret__copy__node__asm_redirect:
	lea	ebp,__interpret__copy__node__asm_redirect_node
	mov	ecx,[ebp]
	# Evaluate the node if necessary
	test	dword ptr [ecx],2
	jne	__interpret__copy__node__asm_redirect_finish
	call	[ecx]
__interpret__copy__node__asm_redirect_finish:
	ret

.global __interpret__copy__node__asm__n
__interpret__copy__node__asm__n:
	save_registers
	# Get interpretation_environment
	mov	ebp,[edx+4]
	mov	ebp,[ebp+8]
	mov	ebp,[ebp+4]
	push	ebp

	# Prepare for calling C function
	mov	ebx,esp
	sub	esp,4
	and	esp,-16
	mov	[esp],eax

	save_host_status_via_ebp
	mov	[ebp+8],edx

	# Parameters are already in the right register; see copy_interpreter_to_host.c
	emms
	call	copy_interpreter_to_host_n
	mov	esp,ebx
	jmp	__interpret__copy__node__asm_finish

.global __interpret__garbage__collect
	# Call as __interpret__garbage__collect (ie) with fastcall convention
__interpret__garbage__collect:
	push	esi
	push	eax
	push	ebx
	push	ebp
	push	edi

	mov	edi,ecx
	push	edi
	restore_host_status_via_edi
	call	collect_0
	pop	ebp
	save_host_status_via_ebp

	pop	edi
	pop	ebp
	pop	ebx
	pop	eax
	pop	esi
	ret

.global __interpret__evaluate__host
	# Call as __interpret__evaluate__host (ie,node)
__interpret__evaluate__host:
	push	ebx
	push	ebp
	push	edi

	mov	edi,ecx
	push	edi
	mov	ecx,edx
	restore_host_status_via_edi
	call	[ecx]

	pop	ebp
	save_host_status_via_ebp

	pop	edi
	pop	ebp
	pop	ebx

	mov	eax,ecx
	ret

.global __interpret__evaluate__host_with_args
	# Call as __interpret__evaluate__host_with_args (arg1,node,ie,ap_address)
__interpret__evaluate__host_with_args:
	sub	esp,20
	mov	[esp+16],ebx
	mov	[esp+12],ebp
	mov	[esp+8],esi
	mov	[esp+4],edi

	mov	edi,[esp+24]
	mov	ebp,[esp+28]
	mov	[esp],edi
	restore_host_status_via_edi
	call	ebp

	add	esp,20
	mov	ebp,[esp-20]
	save_host_status_via_ebp

	mov	edi,[esp-16]
	mov	esi,[esp-12]
	mov	ebp,[esp-8]
	mov	ebx,[esp-4]

	mov	eax,ecx
	ret	8

.global __interpret__add__shared__node
	# Call as __interpret__add__shared__node (Clean_IE,node);
	# Clean_IE should be on top of the A-stack as well.
__interpret__add__shared__node:
	push	ebx
	push	ebp
	push	esi
	push	edi

	# Get interpretation_environment from InterpretationEnvironment
	mov	edi,[edx+4]
	mov	edi,[edi+8]
	mov	edi,[edi+4]
	push	edi
	restore_host_status_via_edi

	# Get shared nodes & ptr from InterpretationEnvironment
	mov	ebp,[edx+8]
	mov	edx,[ebp]
	mov	eax,[ebp+4]

	call	e__ABC_PInterpreter_PInternal__sadd__shared__node

	pop	ebp
	save_host_status_via_ebp

	mov	ebp,[esi-4]
	mov	ebp,[ebp+8]
	mov	[ebp],ecx
	mov	[ebp+4],eax
	mov	eax,ebx

	pop	edi
	pop	esi
	pop	ebp
	pop	ebx
	ret
