#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "abc_instructions.h"
#include "bcprelink.h"
#include "util.h"
#include "parse.h"

const char usage[] = "Usage: %s FILE [-o FILE]\n";

enum section_type {
	ST_Preamble,
	ST_Code,
	ST_Data,
	ST_Start
};

#define _1char2int(a)                ((uint64_t) a)
#define _2chars2int(a,b)             ((uint64_t) (a+(b<<8)))
#define _3chars2int(a,b,c)           ((uint64_t) (a+(b<<8)+(c<<16)))
#define _4chars2int(a,b,c,d)         ((uint64_t) (a+(b<<8)+(c<<16)+(d<<24)))
#define _5chars2int(a,b,c,d,e)       ((uint64_t) (a+(b<<8)+(c<<16)+(d<<24)+((uint64_t)e<<32)))
#define _6chars2int(a,b,c,d,e,f)     ((uint64_t) (a+(b<<8)+(c<<16)+(d<<24)+((uint64_t)e<<32)+((uint64_t)f<<40)))
#define _7chars2int(a,b,c,d,e,f,g)   ((uint64_t) (a+(b<<8)+(c<<16)+(d<<24)+((uint64_t)e<<32)+((uint64_t)f<<40)+((uint64_t)g<<48)))
#define _8chars2int(a,b,c,d,e,f,g,h) ((uint64_t) (a+(b<<8)+(c<<16)+(d<<24)+((uint64_t)e<<32)+((uint64_t)f<<40)+((uint64_t)g<<48)+((uint64_t)h<<56)))

/* The descriptors here should match the order of the descriptors in in _system.abc. */
uint64_t prelinker_preamble[1135] = {
	/*  0 unused; old */ 0, 0, 0, 0, 0,
	/*  5 unused; old */ 0, 0, 0, 0, 0,
	/* 10 unused; old */ 0, 0, 0, 0, 0,
	/* 15 unused; old */ 0, 0, 0, 0, 0,
	/* 20 unused; old */ 0, 0, 0, 0, 0,
	/* 25 unused; old */ 0, 0, 0, 0, 0,
	/* 30 small integers */
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,
	/* 96 */ 0, 97*8, /* caf list */
	/* 98 */
		Cjmp_ap1,  Cjmp_ap2,  Cjmp_ap3,  Cjmp_ap4,  Cjmp_ap5,
		Cjmp_ap6,  Cjmp_ap7,  Cjmp_ap8,  Cjmp_ap9,  Cjmp_ap10,
		Cjmp_ap11, Cjmp_ap12, Cjmp_ap13, Cjmp_ap14, Cjmp_ap15,
		Cjmp_ap16, Cjmp_ap17, Cjmp_ap18, Cjmp_ap19, Cjmp_ap20,
		Cjmp_ap21, Cjmp_ap22, Cjmp_ap23, Cjmp_ap24, Cjmp_ap25,
		Cjmp_ap26, Cjmp_ap27, Cjmp_ap28, Cjmp_ap29, Cjmp_ap30,
		Cjmp_ap31, Cjmp_ap32,
	/* 130 unused; old */ 0,
	/* 131 */ Cjsr_eval0, Cfill_a01_pop_rtn, Chalt, Chalt, -2, /* indirection */
		Cpush_node1, (690+1)*8, Cjsr_eval0, Cfill_a01_pop_rtn,
	/* 140 static _Nil node + _Nil descriptor */
		141*8+2, 0, 0, 0, 4, _4chars2int('_','N','i','l'),
	/* 146 static characters*/
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
	/* 658 */ Cinstruction, 0, /* to return to JavaScript */
	/* 660 static booleans */
		PRELINKED_DESC_BOOL*8+2,0, PRELINKED_DESC_BOOL*8+2,1,
	/* 664 FILE descriptor */
		258, 2, _2chars2int('i','i'), 4, _4chars2int('F','I','L','E'),
	/* 669 power10_table (for RtoAC) */
		0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
	/* 685 power10_table2 (for RtoAC) */
		0,0,0,0,0,
	/* 690 cycle in spine*/
		0, Chalt,
	/* 692 unused, reserved for future use*/
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
	/* 1000 */ 0, 0, 0, 7,  _7chars2int('_','A','R','R','A','Y','_'),
	/* 1005 */ 0, 0, 0, 9,  _8chars2int('_','A','R','R','A','Y','P','2'), _1char2int('_'),
	/* 1011 */ 0, 0, 0, 9,  _8chars2int('_','A','R','R','A','Y','_','R'), _1char2int('_'),
	/* 1017 */ 0, 0, 0, 12, _8chars2int('_','A','R','R','A','Y','_','R'), _4chars2int('E','A','L','_'),
	/* 1023 */ 0, 0, 0, 13, _8chars2int('_','A','R','R','A','Y','_','D'), _5chars2int('R','E','A','L','_'),
	/* 1029 */ 0, 0, 0, 11, _8chars2int('_','A','R','R','A','Y','_','I'), _3chars2int('N','T','_'),
	/* 1035 */ 0, 0, 0, 14, _8chars2int('_','A','R','R','A','Y','_','R'), _6chars2int('E','A','L','3','2','_'),
	/* 1041 */ 0, 0, 0, 13, _8chars2int('_','A','R','R','A','Y','_','I'), _5chars2int('N','T','3','2','_'),
	/* 1047 */ 0, 0, 0, 12, _8chars2int('_','A','R','R','A','Y','_','B'), _4chars2int('O','O','L','_'),
	/* 1053 */ 0, 0, 0, 14, _8chars2int('_','A','R','R','A','Y','P','2'), _6chars2int('_','R','E','A','L','_'),
	/* 1059 */ 0, 0, 0, 15, _8chars2int('_','A','R','R','A','Y','P','2'), _7chars2int('_','D','R','E','A','L','_'),
	/* 1065 */ 0, 0, 0, 13, _8chars2int('_','A','R','R','A','Y','P','2'), _5chars2int('_','I','N','T','_'),
	/* 1071 */ 0, 0, 0, 16, _8chars2int('_','A','R','R','A','Y','P','2'), _8chars2int('_','R','E','A','L','3','2','_'),
	/* 1077 */ 0, 0, 0, 15, _8chars2int('_','A','R','R','A','Y','P','2'), _7chars2int('_','I','N','T','3','2','_'),
	/* 1083 */ 0, 0, 0, 14, _8chars2int('_','A','R','R','A','Y','P','2'), _6chars2int('_','B','O','O','L', '_'),
	/* 1089 */ 0, 0, 0, 14, _8chars2int('_','A','R','R','A','Y','P','2'), _6chars2int('_','C','H','A','R', '_'),
	/* 1095 */ 0, 0, 0, 8,  _8chars2int('_','S','T','R','I','N','G','_'),
	/* 1100 */ 0, 0, 0, 4,  _4chars2int('R','E','A','L'),
	/* 1105 */ 0, 0, 0, 5,  _5chars2int('D','R','E','A','L'),
	/* 1110 */ 0, 0, 0, 3,  _3chars2int('I','N','T'),
	/* 1115 */ 0, 0, 0, 6,  _6chars2int('R','E','A','L', '3', '2'),
	/* 1120 */ 0, 0, 0, 5,  _5chars2int('I','N','T', '3', '2'),
	/* 1125 */ 0, 0, 0, 4,  _4chars2int('B','O','O','L'),
	/* 1130 */ 0, 0, 0, 4,  _4chars2int('C','H','A','R'),
};

void prepare_preamble(void) {
	/* small integers */
	for (int i=0; i<=32; i++) {
		prelinker_preamble[30+i*2]=PRELINKED_DESC_INT*8+2;
		prelinker_preamble[30+i*2+1]=i;
	}

	/* static characters */
	for (int i=0; i<256; i++) {
		prelinker_preamble[146+i*2]=PRELINKED_DESC_CHAR*8+2;
		prelinker_preamble[146+i*2+1]=i;
	}

	/* power10_table */
	prelinker_preamble[669]=((union double_and_int){.d=1.0e00}).i;
	prelinker_preamble[670]=((union double_and_int){.d=1.0e01}).i;
	prelinker_preamble[671]=((union double_and_int){.d=1.0e02}).i;
	prelinker_preamble[672]=((union double_and_int){.d=1.0e03}).i;
	prelinker_preamble[673]=((union double_and_int){.d=1.0e04}).i;
	prelinker_preamble[674]=((union double_and_int){.d=1.0e05}).i;
	prelinker_preamble[675]=((union double_and_int){.d=1.0e06}).i;
	prelinker_preamble[676]=((union double_and_int){.d=1.0e07}).i;
	prelinker_preamble[677]=((union double_and_int){.d=1.0e08}).i;
	prelinker_preamble[678]=((union double_and_int){.d=1.0e09}).i;
	prelinker_preamble[679]=((union double_and_int){.d=1.0e10}).i;
	prelinker_preamble[680]=((union double_and_int){.d=1.0e11}).i;
	prelinker_preamble[681]=((union double_and_int){.d=1.0e12}).i;
	prelinker_preamble[682]=((union double_and_int){.d=1.0e13}).i;
	prelinker_preamble[683]=((union double_and_int){.d=1.0e14}).i;
	prelinker_preamble[684]=((union double_and_int){.d=1.0e15}).i;

	/* power10_table2 */
	prelinker_preamble[685]=((union double_and_int){.d=1.0e16}).i;
	prelinker_preamble[686]=((union double_and_int){.d=1.0e32}).i;
	prelinker_preamble[687]=((union double_and_int){.d=1.0e64}).i;
	prelinker_preamble[688]=((union double_and_int){.d=1.0e128}).i;
	prelinker_preamble[689]=((union double_and_int){.d=1.0e256}).i;
}

void write_section(FILE *f, enum section_type type, uint32_t len, uint64_t *data) {
	fwrite(&type, 1, sizeof(uint32_t), f);
	fwrite(&len, 1, sizeof(uint32_t), f);
	fwrite(data, sizeof(uint64_t), len, f);
}

static uint32_t varwidth_encode(uint32_t len, uint64_t *data, uint64_t **dest) {
	char *ptr=safe_calloc (len*sizeof(uint64_t)*5/4,sizeof(char)); /* rough upper bound */
	*dest=(uint64_t*)ptr;

	*(uint32_t*)ptr=len;
	ptr+=4;

	while (len--) {
		uint64_t val=*data++;
		char byte=0x00;

		if ((int64_t)val<0) {
			val=0-(int64_t)val;
			byte=0x40;
		}

		byte|=val&0x3f;
		val>>=6;
		if (val)
			byte|=0x80;
		*ptr++=byte;

		while (val) {
			byte=val&0x7f;
			val>>=7;
			if (val)
				byte|=0x80;
			*ptr++=byte;
		}
	}

	return ((ptr-(char*)*dest)+7)>>3;
}

static void write_section_varwidth(FILE *f, enum section_type type, uint32_t len, uint64_t *data) {
	uint64_t *leb_encoded_data;
	uint32_t leb_encoded_len=varwidth_encode (len,data,&leb_encoded_data);
	write_section (f,type,leb_encoded_len,leb_encoded_data);
	free (leb_encoded_data);
}

static void write_header(FILE *f) {
	uint32_t v=ABC_PRELINKED_MAGIC_NUMBER;
	fwrite(&v, sizeof(uint32_t), 1, f);
	v=ABC_PRELINKED_VERSION;
	fwrite(&v, sizeof(uint32_t), 1, f);
	v=ABC_VERSION;
	fwrite(&v, sizeof(uint32_t), 1, f);
}

int main(int argc, char **argv) {
	char *output_file_name=NULL;
	FILE *input_file=NULL;

	for (int i=1; i<argc; i++) {
		if(!strcmp("-o", argv[i]) && i <= argc-1) {
			output_file_name=argv[i+1];
			i++;
		} else if (input_file!=NULL) {
			fprintf(stderr, usage, argv[0]);
			return -1;
		} else {
			input_file=fopen(argv[i], "rb");
			if (input_file==NULL) {
				fprintf(stderr, "Error: Could not open input file: %s\n", argv[i]);
				return -1;
			}
		}
	}

	if (input_file==NULL) {
		fprintf(stderr, usage, argv[0]);
		return -1;
	}

	struct parser state;
	init_parser(&state);

	struct char_provider cp;
	new_file_char_provider(&cp, input_file);
	int res=parse_program(&state, &cp);
	free_parser(&state);
	free_char_provider(&cp);
	if (res != PARSE_PROGRAM_NO_ERROR) {
		EPRINTF("Parsing failed (%s)\n", parse_program_strerror(res));
		return res;
	}

	FILE *output_file=stdout;
	if(output_file_name!=NULL && (output_file=fopen(output_file_name, "wb"))==NULL) {
		fprintf(stderr, "Error: Could not open output file: %s\n", output_file_name);
		return -1;
	}

	struct program *program=state.program;

	prepare_preamble();
	write_header(output_file);
	write_section_varwidth(output_file, ST_Preamble, sizeof(prelinker_preamble)/sizeof(uint64_t), prelinker_preamble);
	write_section_varwidth(output_file, ST_Code, program->code_size, program->code);
	write_section_varwidth(output_file, ST_Data, program->data_size, program->data);
	if (program->symbol_table_size>0)
		write_section(output_file, ST_Start, 1, &program->symbol_table[program->start_symbol_id].offset);

	free_program(program);
	free(program);

	fclose(output_file);

	return 0;
}
