#pragma once

#include "bytecode.h"

struct interpretation_options {
	int in_first_semispace:1;
	int allow_file_io:1;
#ifdef LINK_CLEAN_RUNTIME
	int hyperstrict:1;
#endif
};

#ifdef LINK_CLEAN_RUNTIME
/* This struct matches the Clean structure that the interpretation_environment
 * is kept in. */
struct InterpretationEnvironment2 {
	BC_WORD **__ie_shared_nodes;
	int __ie_shared_nodes_ptr;
};
struct InterpretationEnvironment {
	void *__ie_descriptor;
	struct finalizers *__ie_finalizer;
	struct InterpretationEnvironment2 *__ie_2;
};

#if WORD_WIDTH==32
extern BC_WORD *end_heap;
#endif

struct host_status {
	BC_WORD *host_a_ptr;  /* The A-stack pointer of the host */
	BC_WORD *host_hp_ptr; /* Heap pointer */
#if WORD_WIDTH==64
	size_t host_hp_free;  /* Nr. of free heap words */
#endif
	struct InterpretationEnvironment *clean_ie; /* Clean InterpretationEnvironment */
};

struct interpretation_environment {
	struct host_status *host;
	struct program *program;
	BC_WORD *heap;
	BC_WORD heap_size;
	BC_WORD *stack;
	BC_WORD stack_size;
	BC_WORD *asp;
	BC_WORD *bsp;
	BC_WORD *csp;
	BC_WORD *hp;
	BC_WORD *hp_end;
	void *caf_list[2];
	struct interpretation_options options;
};

extern void *e__ABC_PInterpreter__dDV__ParseError;
extern void *e__ABC_PInterpreter__dDV__HeapFull;
extern void *e__ABC_PInterpreter__dDV__StackOverflow;
extern void *e__ABC_PInterpreter__dDV__FloatingPointException;
extern void *e__ABC_PInterpreter__dDV__Halt;
extern void *e__ABC_PInterpreter__dDV__IllegalInstruction;
extern void *e__ABC_PInterpreter__dDV__FileIOAttempted;
extern void *e__ABC_PInterpreter__dDV__SegmentationFault;
extern void *e__ABC_PInterpreter__dDV__HostHeapFull;
extern void *e__ABC_PInterpreter__kDV__Ok;

extern void **interpret_error;
#endif
#ifdef DEBUG_CURSES
extern void** ARRAY;
#endif
extern void* descriptors[];
#define ADDR_d__Nil (&descriptors[1])
#define ADDR_d_FILE (&descriptors[6])
#define ADDR__ARRAY_DREAL_ (&descriptors[IF_INT_64_OR_32(35,39)])
#define ADDR__ARRAYP2_DREAL_ (&descriptors[IF_INT_64_OR_32(71,85)])
#define ADDR_DREAL (&descriptors[IF_INT_64_OR_32(117,144)])
#ifdef LINK_CLEAN_RUNTIME
extern void* __ARRAY__;
extern void* __ARRAY__R__;
extern void* __ARRAY__REAL__;
extern void* __ARRAY__INT__;
extern void* __ARRAY__REAL32_;
extern void* __ARRAY__INT32_;
extern void* __ARRAY__BOOL__;
extern void* __ARRAYP2__REAL__;
extern void* __ARRAYP2__INT__;
extern void* __ARRAYP2__REAL32__;
extern void* __ARRAYP2__INT32__;
extern void* __ARRAYP2__BOOL__;
extern void* __ARRAYP2__CHAR__;
extern void* __STRING__;
extern void* REAL;
extern void *INT;
extern void *REAL32;
// Has a _ suffix to avoid name clash with Windows INT32 type.
extern void *INT32_;
extern void* BOOL;
extern void* CHAR;
# define ADDR__ARRAY_ (&__ARRAY__)
# define ADDR__ARRAY_R_ (&__ARRAY__R__)
# define ADDR__ARRAYP2_ (&__ARRAYP2__)
# define ADDR__ARRAY_REAL_ (&__ARRAY__REAL__)
# define ADDR__ARRAY_INT_ (&__ARRAY__INT__)
# define ADDR__ARRAY_REAL32_ (&__ARRAY__REAL32__)
# define ADDR__ARRAY_INT32_ (&__ARRAY__INT32__)
# define ADDR__ARRAY_BOOL_ (&__ARRAY__BOOL__)
# define ADDR__ARRAYP2_REAL_ (&__ARRAYP2__REAL__)
# define ADDR__ARRAYP2_INT_ (&__ARRAYP2__INT__)
# define ADDR__ARRAYP2_REAL32_ (&__ARRAYP2__REAL32__)
# define ADDR__ARRAYP2_INT32_ (&__ARRAYP2__INT32__)
# define ADDR__ARRAYP2_BOOL_ (&__ARRAYP2__BOOL__)
# define ADDR__ARRAYP2_CHAR_ (&__ARRAYP2__CHAR__)
# define ADDR__STRING_ (&__STRING__)
# define ADDR_REAL (&REAL)
# define ADDR_INT (&INT)
# define ADDR_REAL32 (&REAL32)
# define ADDR_INT32 (&INT32_)
# define ADDR_BOOL (&BOOL)
# define ADDR_CHAR (&CHAR)
#else
# define ADDR__ARRAY_ (&descriptors[12])
# define ADDR__ARRAY_R_ (&descriptors[IF_INT_64_OR_32(23,25)])
# define ADDR__ARRAYP2_ (&descriptors[IF_INT_64_OR_32(17,18)])
# define ADDR__ARRAY_REAL_ (&descriptors[IF_INT_64_OR_32(29,32)])
# define ADDR__ARRAY_INT_ (&descriptors[IF_INT_64_OR_32(41,47)])
# define ADDR__ARRAY_REAL32_ (&descriptors[IF_INT_64_OR_32(47,54)])
# define ADDR__ARRAY_INT32_ (&descriptors[IF_INT_64_OR_32(53,62)])
# define ADDR__ARRAY_BOOL_ (&descriptors[IF_INT_64_OR_32(59,70)])
# define ADDR__ARRAYP2_REAL_ (&descriptors[IF_INT_64_OR_32(64,77)])
# define ADDR__ARRAYP2_INT_ (&descriptors[IF_INT_64_OR_32(77,93)])
# define ADDR__ARRAYP2_REAL32_ (&descriptors[IF_INT_64_OR_32(83,101)])
# define ADDR__ARRAYP2_INT32_ (&descriptors[IF_INT_64_OR_32(89,109)])
# define ADDR__ARRAYP2_BOOL_ (&descriptors[IF_INT_64_OR_32(95,117)])
# define ADDR__ARRAYP2_CHAR_ (&descriptors[IF_INT_64_OR_32(101,125)])
# define ADDR__STRING_ (&descriptors[IF_INT_64_OR_32(107,133)])
# define ADDR_REAL (&descriptors[IF_INT_64_OR_32(112,139)])
# define ADDR_INT (&descriptors[IF_INT_64_OR_32(122,150)])
# define ADDR_REAL32 (&descriptors[IF_INT_64_OR_32(127,155)])
# define ADDR_INT32 (&descriptors[IF_INT_64_OR_32(132,161)])
# define ADDR_BOOL (&descriptors[IF_INT_64_OR_32(137,167)])
# define ADDR_CHAR (&descriptors[IF_INT_64_OR_32(142,172)])
#endif

extern BC_WORD small_integers[66];
extern BC_WORD static_characters[512];
extern BC_WORD static_booleans[4];

#ifdef LINK_CLEAN_RUNTIME
#include "copy_interpreter_to_host.h"
extern void **HOST_NODES[32];
extern BC_WORD HOST_NODE_DESCRIPTORS[1216];
extern BC_WORD HOST_NODE_INSTRUCTIONS[32*6];
#endif

extern BC_WORD Fjmp_ap[32];

extern void* __interpreter_cycle_in_spine[2];
extern void* __interpreter_indirection[9];

#define A_STACK_CANARY 0x87654321 /* random value to check whether the A-stack overflew */

#ifdef COMPUTED_GOTOS
# include "abc_instructions.h"
extern void *instruction_labels[CMAX];
#endif

/**
 * program: the program to run
 * stack: stack block
 * stack_size: size of stack in words
 * heap: heap block
 * heap_size: size of heap in words
 * asp: A-stack pointer address (e.g. stack)
 * bsp: B-stack pointer address (e.g. &stack[stack_size])
 * csp: C-stack pointer address (e.g. &stack[stack_size >> 1])
 * hp: pointer to the next free block on the heap
 * node:
 *  - Pointer to a node to evaluate to HNF;
 *  - NULL if we should just start running at code[0].
 */
int interpret(
#ifdef LINK_CLEAN_RUNTIME
		struct interpretation_environment *ie,
		int create_restore_point,
#else
		struct program *program,
		struct interpretation_options options,
#endif
		BC_WORD *stack, size_t stack_size,
		BC_WORD *heap, size_t heap_size,
		BC_WORD *asp, BC_WORD *bsp, BC_WORD *csp, BC_WORD *hp,
		BC_WORD *node);
