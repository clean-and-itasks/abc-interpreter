#pragma once

#define PRELINKED_DESC__Nil 141
#define PRELINKED_DESC__ARRAY_ 1000
#define PRELINKED_DESC__ARRAY_R_ 1011
#define PRELINKED_DESC__ARRAY_REAL_ 1017
#define PRELINKED_DESC__ARRAY_DREAL_ 1023
#define PRELINKED_DESC__ARRAY_INT_ 1029
#define PRELINKED_DESC__ARRAY_BOOL_ 1047
#define PRELINKED_DESC__STRING_ 1095
#define PRELINKED_DESC_REAL 1100
#define PRELINKED_DESC_DREAL 1105
#define PRELINKED_DESC_INT 1110
#define PRELINKED_DESC_BOOL 1125
#define PRELINKED_DESC_CHAR 1130

extern uint64_t prelinker_preamble[1135];
