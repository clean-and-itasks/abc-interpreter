# Contributing to the ABC Interpreter

Thank you for contributing to the ABC interpreter.
This document describes how we work here.

[[_TOC_]]

## General workflow

- Bugs and feature proposals are tracked in the issue tracker.
- Once code is ready to be merged into master this is done in a merge request.
- Merge requests are assigned to the person who currently has to work on it.
  This can be a reviewer when the code is ready for review, or somebody else,
  if more work is required before the MR can be merged. Merge requests that are
  not ready for review should also be marked as Drafts.
- Comment threads are resolved by the person who started them, not the person
  who answers them. This makes sure that everyone has an overview of what
  remains to be done.

## Testing

- Existing tests should be adapted if required so that the CI pipeline passes.
- For fixed bugs there should always be at least one test, which failed before
  the MR and passes after the MR.

## Documentation

- All changed/added exported functions and types should be correctly documented
  in the definition module according to the
  [Platform documentation standards](https://gitlab.com/clean-and-itasks/clean-platform/-/blob/master/doc/DOCUMENTATION.md).
- In implementation modules unusual choices (e.g. fancy optimisations) or parts
  which are likely to get broken in the future (e.g. arguments which have to
  remain lazy) should be documented.
- *What* code does should become clear from the code itself. Good structure and
  naming is important to achieve this. Comments should explain *why* certain
  non-straightforward choices have been made.
