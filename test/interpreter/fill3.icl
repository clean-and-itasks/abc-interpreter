module fill3

/**
 * This tests all variantes generated for `fill3` and `fill3_r` instructions. It seems that all variants are only
 * generated for updating unique records. There is a test updating an ADT, as a different descriptor offset has to be
 * used in this case.
 */

import Data.Func
import StdEnv

Start =
    ( updAdt (C 1 2 3), sumOfFields $ upda01 init, sumOfFields $ upda0 init, sumOfFields $ upda1 init
    , sumOfFields $ updr01 init, sumOfFields $ updr0 init, sumOfFields $ updra init, sumOfFieldsB $ updrb initB)

:: *T = C Int Int Int

updAdt :: !*T -> *T
updAdt (C a b c) = C a b 0

// A unique record (as for such `fill3_r` instructions are generated); `field2` is a B argument.
:: *Rec = ! {field1 :: Int, field2 :: !Int, field3 :: Int, field4 :: Int, field5 :: Int}

// A unique record with only B arguments.
:: *RecB = ! {field1 :: !Int, field2 :: !Int, field3 :: !Int, field4 :: !Int, field5 :: !Int}

init :: *Rec
init = {Rec| field1 = 1, field2 = 2, field3 = 4, field4 = 8, field5 = 16}

sumOfFields :: !*Rec -> Int
sumOfFields {Rec| field1, field2, field3, field4, field5} = field1 + field2 + field3 + field4 + field5

initB:: *RecB
initB = {RecB| field1 = 1, field2 = 2, field3 = 4, field4 = 8, field5 = 16}

sumOfFieldsB :: !*RecB -> Int
sumOfFieldsB {RecB| field1, field2, field3, field4, field5} = field1 + field2 + field3 + field4 + field5

// generates a `fill3a01`
upda01 :: !*Rec -> *Rec
upda01 r = {Rec| r & field3 = 0}

// generates a `fill3a0`
upda0 :: !*Rec -> *Rec
upda0 r = {Rec| r & field3 = 0, field4 = 32}

// generates a `fill3a1`
upda1 :: !*Rec -> *Rec
upda1 r = {Rec| r & field1 = 0, field3 = 32}

// generates a `fill3r01`
updr01 :: !*Rec -> *Rec
updr01 r = {Rec| r & field2 = 0}

// generates a `fill3r0`
updr0 :: !*Rec-> *Rec
updr0 r = {Rec | r & field2 = 0, field3 = 32}

// generates a `fill3ra`
updra :: !*Rec-> *Rec
updra r = {Rec | r & field1 = 0, field2 = 32}

// generates a `fill3rb`
updrb :: !*RecB-> *RecB
updrb r = {RecB | r & field1 = 0, field4 = 32}
