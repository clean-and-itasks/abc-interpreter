#!/bin/bash

# Runs tests from the same directory and checks the outcome against the
# .expected file.
#
# In principle, the outcome for MODULE.icl is in MODULE.expected. However, in
# cases where the outcome depends on the platform, this can be overridden. For
# instance, MODULE.64_32.expected is the outcome when MODULE is compiled with
# 64-bit Clean and run on a 32-bit interpreter.

IP=../../src/interpret
[ "$OS" == "Windows_NT" ] && IP=../../src/interpret.exe

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
PURPLE="\033[0;35m"
RESET="\033[0m"

FAILED=()

RUNFLAGS=""
NATIVE_RUNFLAGS=""

WASM=0

BENCHMARK=0
EXPECTED_PREFIX="64"
BC_EXTENSION="bc"
RUN_ONLY=()
QUIET=0
JUNIT_EXPORT=0

junit_export () {
	MODULE="$1"
	RESULT="$2"
	ELAPSED_TIME="$3"
	EXPECTED_FILE="$4"
	FAILURES=0
	if [ "$RESULT" = "failed" ]; then
		FAILURES=1
	fi

	echo "<?xml version=\"1.0\"?>"
	echo "<testsuites tests=\"1\" failures=\"$FAILURES\" time=\"$ELAPSED_TIME\">"
	echo "<testsuite name=\"$MODULE\" tests=\"1\" failures=\"$FAILURES\" time=\"$ELAPSED_TIME\">"
	echo "<testcase id=\"$MODULE\" name=\"$MODULE\" classname=\"$MODULE\" time=\"$ELAPSED_TIME\">"
	if [ "$RESULT" = "failed" ]; then
		echo "<failure>"
		git diff --no-index --word-diff --word-diff-regex='\w+' -U0 $EXPECTED $MODULE.result \
			| sed 's/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g'
		echo "</failure>"
	fi
	echo "</testcase>"
	echo "</testsuite>"
	echo "</testsuites>"
}

print_help () {
	echo "$0: run tests"
	echo
	echo "Options:"
	echo "  -h       Print this help"
	echo
	echo "  -o TEST  Only run test TEST"
	echo "  -q       Don't show program results"
	echo "  -b       Run benchmarks"
	echo
	echo "  -w       Use the WebAssembly interpreter (does not support all options below)"
	echo "  -3       Run tests as if on a 32-bit machine"
	echo
	echo "  -l       List bytecode before execution"
	echo "  -j       Output JUnit style XML files (for GitLab CI)"
	exit 0
}

print_usage () {
	echo "Usage: $0 OPTS (see -h for details)"
	exit 1
}

contains () {
	local e match="$1"
	shift
	for e; do [[ "$e" == "$match" ]] && return 0; done
	return 1
}

OPTS=`getopt "ho:qbw3lj" "$@"` || print_usage
eval set -- "$OPTS"

while true; do
	case "$1" in
		-h)
			print_help;;

		-o)
			RUN_ONLY+=("$2")
			shift 2;;

		-w)
			WASM=1
			# V8 10.2 enabled dynamic tiering which means benchmarks typically
			# won't run in TurboFan but in LiftOff. Disable this feature to get
			# reliable benchmarks using optimized code.
			IP="node --nowasm-dynamic-tiering ../../src-js/interpret.js"
			BC_EXTENSION="pbc"
			shift;;

		-b)
			BENCHMARK=1
			shift;;
		-3)
			EXPECTED_PREFIX="32"
			shift;;

		-l)
			RUNFLAGS+=" -l"
			shift;;
		-j)
			JUNIT_EXPORT=1
			shift;;
		-q)
			QUIET=1
			shift;;

		--)
			shift
			break;;
		*)
			break;;
	esac
done

if [ $WASM -gt 0 ] && [ $BENCHMARK -gt 0 ]; then
	RUNFLAGS+=" --time"
fi

CLMOPTS="-I $CLEAN_HOME/../base-stdenv/lib -I $CLEAN_HOME/../base-rts/lib -dynamics"
clm $CLMOPTS -nt is_32_bit_clean -o is_32_bit_clean
if [[ "$(./is_32_bit_clean)" == "True" ]]; then
	EXPECTED_PREFIX=".32_$EXPECTED_PREFIX"
else
	EXPECTED_PREFIX=".64_$EXPECTED_PREFIX"
fi

clm $CLMOPTS -optabc -bytecode -O _system

for MODULE in *.icl
do
	MODULE="${MODULE/.icl/}"

	if [[ "$MODULE" == "is_32_bit_clean" ]]; then
		continue
	elif [[ "$MODULE" == "long_integers" ]] && [ $WASM -gt 0 ]; then
		continue
	elif [[ "${#RUN_ONLY[@]}" -gt 0 ]] && ! contains "$MODULE" "${RUN_ONLY[@]}"; then
		continue
	fi

	if [ $BENCHMARK -gt 0 ]; then
		cp "$MODULE.icl" "$MODULE.icl.nobm"
		if [ -f "$MODULE.bm.sed" ]; then
			sed -i.nobm -f "$MODULE.bm.sed" "$MODULE.icl"
		fi
	fi

	echo -e "${YELLOW}Running $MODULE...$RESET"

	CLMOPTS="
		-I $CLEAN_HOME/../base-lib/lib
		-I $CLEAN_HOME/../base-rts/lib
		-I $CLEAN_HOME/../base-stdenv/lib
		-I $CLEAN_HOME/../category-theory/lib
		-I $CLEAN_HOME/../generic-binary-encoding/lib
		-I $CLEAN_HOME/../integer/lib
		-dynamics -optabc -bytecode"
	if [ $WASM -gt 0 ]; then
		CLMOPTS="$CLMOPTS -prelink-bytecode"
	fi
	if [ -f "$MODULE.options" ]; then
		CLMOPTS="$CLMOPTS $(cat "$MODULE.options")"
	fi
	clm $CLMOPTS "$MODULE" -o "$MODULE"
	CLMRESULT=$?

	if [ $CLMRESULT -ne 0 ]; then
		echo -e "${RED}FAILED: $MODULE (compilation)$RESET"
		# The touch is needed to make sure that the ABC is regenerated next time
		[ $BENCHMARK -gt 0 ] && mv "$MODULE.icl.nobm" "$MODULE.icl" && touch "$MODULE.icl"
		FAILED+=("$MODULE")
		continue
	fi

	EXPECTED="$MODULE$EXPECTED_PREFIX.expected"
	if [ ! -f "$MODULE$EXPECTED_PREFIX.expected" ]; then
		if [ -f "$MODULE.expected" ]; then
			EXPECTED="$MODULE.expected"
		else
			echo -e "${YELLOW}Skipping $MODULE (no expected outcome)${RESET}"
			[ $BENCHMARK -gt 0 ] && mv "$MODULE.icl.nobm" "$MODULE.icl"
			continue
		fi
	fi

	MODULE_RUNFLAGS=""
	if [ -f "$MODULE.options" ]; then
		MODULE_RUNFLAGS="$(cat "$MODULE.options")"
	fi

	[ $BENCHMARK -gt 0 ] && mv "$MODULE.icl.nobm" "$MODULE.icl"

	if [ $BENCHMARK -gt 0 ]; then
		/usr/bin/time -p $IP $MODULE_RUNFLAGS $RUNFLAGS $MODULE.$BC_EXTENSION 2>bm-tmp >$MODULE.result
		WALL_TIME="$(grep user bm-tmp | sed 's/user[[:space:]]*//' | head -n 1)"
		/usr/bin/time -p ./"$MODULE" $MODULE_RUNFLAGS $NATIVE_RUNFLAGS -nt -nr 2>bm-tmp
		WALL_TIME_NATIVE="$(grep user bm-tmp | sed 's/user[[:space:]]*//')"
		rm bm-tmp
		if [ "$WALL_TIME_NATIVE" == "0.00" ]; then
			WALL_TIME_RATIO="ratio not computable"
		else
			WALL_TIME_RATIO="$(echo "scale=3;$WALL_TIME/$WALL_TIME_NATIVE" | bc)x"
		fi
		echo -e "${PURPLE}Time used: $WALL_TIME / $WALL_TIME_NATIVE (${WALL_TIME_RATIO})$RESET"
	else
		if [ $QUIET -gt 0 ]; then
			/usr/bin/time $IP $MODULE_RUNFLAGS $RUNFLAGS $MODULE.$BC_EXTENSION 2>time-tmp > $MODULE.result
		else
			/usr/bin/time $IP $MODULE_RUNFLAGS $RUNFLAGS $MODULE.$BC_EXTENSION 2>time-tmp | tee $MODULE.result
		fi

		cat time-tmp > /dev/stderr
		WALL_TIME="$(head -n 1 time-tmp | cut -du -f1)"
		rm time-tmp
	fi

	[ "$OS" == "Windows_NT" ] && dos2unix $MODULE.result

	if [ $BENCHMARK -gt 0 ]; then
		if [ -f "$MODULE.bm$EXPECTED_PREFIX.expected" ]; then
			EXPECTED="$MODULE.bm$EXPECTED_PREFIX.expected"
		elif [ -f "$MODULE.bm.expected" ]; then
			EXPECTED="$MODULE.bm.expected"
		fi
	fi
	git --no-pager diff --no-index --word-diff --word-diff-regex='\w+' -U0 $EXPECTED $MODULE.result
	if [ $? -ne 0 ]; then
		[ $JUNIT_EXPORT -gt 0 ] && junit_export $MODULE failed "$WALL_TIME" "$EXPECTED" > "$MODULE.junit.xml"
		echo -e "${RED}FAILED: $MODULE (different result)$RESET"
		FAILED+=("$MODULE")
	else
		[ $JUNIT_EXPORT -gt 0 ] && junit_export "$MODULE" passed "$WALL_TIME" "$EXPECTED" > "$MODULE.junit.xml"
		echo -e "${GREEN}Passed: $MODULE$RESET"
	fi
done

if [ ${#FAILED[@]} -eq 0 ]; then
	echo -e "${GREEN}All tests passed$RESET"
else
	echo -e "${RED}Some tests failed: ${FAILED[@]}$RESET"
	exit 1
fi
