module fills

:: *UT
	= UT1_0 Int
	| UT1_1 !Int
	| UT2_01 Int !Int
	| UT2_11 !Int !Int
	| UT3_000 Int Int Int
	| UT3_000a Int Int Int
	| UT3_001 Int Int !Int
	| UT3_011 Int !Int !Int
	| UT3_111 !Int !Int !Int
	| UT4_0000 Int Int Int Int
	| UT4_0001 Int Int Int !Int
	| UT20_a Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int
	| UT20_b Int Int !Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int Int

f :: !Bool !*UT -> *(!Bool, !*UT)
f True  (UT1_0 a) = (True,  UT1_1 a)
f True  (UT1_1 a) = (True,  UT1_0 a)
f False (UT1_0 a) = (False, UT1_0 a)
f False (UT1_1 a) = (True,  UT1_1 a)
f True  (UT2_01 a b) = (True,  UT2_11 b a)
f False (UT2_01 a b) = (False, UT2_01 a a)
f True  (UT2_11 a b) = (False, UT2_01 b a)
f False (UT2_11 a b) = (True,  UT2_11 b b)
f True  (UT3_000 a b c) = (True, UT3_001 a b b)
f False (UT3_000 a b c) = (True, UT3_000 a b b)
f True  (UT3_000a a b c) = (True, UT3_000 0 b c)
f False (UT3_000a a b c) = (True, UT3_000a 0 b c)
f True  (UT3_001 a b c) = (True, UT3_011 a b b)
f False (UT3_001 a b c) = (True, UT3_001 a c b)
f True  (UT3_011 a b c) = (True, UT3_111 c b c)
f False (UT3_011 a b c) = (True, UT3_011 c b a)
f True  (UT3_111 a b c) = (True, UT3_000 a b b)
f False (UT3_111 a b c) = (True, UT3_111 c a b)
f True  (UT4_0001 a b c d) = (True, UT4_0000 a b c d)
f False (UT4_0000 a b c d) = (True, UT4_0000 b b b b)
f True  (UT20_a a b c d e f g h i j k l m n o p q r s t) = (True, UT20_a a b b d e f g h i j k l m n o p q r s b)
f False (UT20_a a b c d e f g h i j k l m n o p q r s t) = (True, UT20_b b b b d e f g h i j k l m n o p q r s b)
f True  (UT20_b a b c d e f g h i j k l m n o p q r s t) = (True, UT20_a a b b d e f g h i j k l m n o p q r s t)
f False (UT20_b a b c d e f g h i j k l m n o p q r s t) = (True, UT20_b a b b d e f g h i j k l m n o p q r s t)

Start =
	[ f True  (UT1_0 1)
	, f False (UT1_0 1)
	, f True  (UT1_1 1)
	, f False (UT1_1 1)
	, f True  (UT2_01 1 2)
	, f False (UT2_01 1 2)
	, f True  (UT2_11 1 2)
	, f False (UT2_11 1 2)
	, f True  (UT3_000 1 2 3)
	, f False (UT3_000 1 2 3)
	, f True  (UT3_000a 1 2 3)
	, f False (UT3_000a 1 2 3)
	, f True  (UT3_001 1 2 3)
	, f False (UT3_001 1 2 3)
	, f True  (UT3_011 1 2 3)
	, f False (UT3_011 1 2 3)
	, f True  (UT3_111 1 2 3)
	, f False (UT3_111 1 2 3)
	, f True  (UT4_0001 1 2 3 4)
	, f False (UT4_0000 1 2 3 4)
	, f True  (UT20_a 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)
	, f False (UT20_a 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)
	, f True  (UT20_b 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)
	, f False (UT20_b 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)
	]
