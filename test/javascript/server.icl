implementation module server

import Base64
import Data.Error
import Data.Func
import Gast.Gen
import StdEnv
import StdMaybe
import System.CommandLine
import System.OS
import System.Process
import System.Time
import System._Unsafe
import TCPIP
import Testing.TestEvents
import Text.GenJSON
import Text.GenPrint

import ABC.Interpreter
import ABC.Interpreter.JavaScript

TIMEOUT :== ?Just 5000

LoadNewByteCode :== 0
RunInitFunction :== 1
RunFunction :== 2

doRequest :: !Int !String !*World -> (!MaybeError String String, !*World)
doRequest message_type req w
	# (?Just ip,w) = lookupIPAddress "localhost" w
	# (rpt,mbChannel,w) = connectTCP_MT TIMEOUT (ip, 20519) w
	| rpt == TR_Expired
		= (Error "connection timed out", w)
	| rpt <> TR_Success
		= (Error "connection failed", w)
	# {sChannel,rChannel} = fromJust mbChannel
	# (rpt,_,sChannel,w) = send_MT TIMEOUT (toByteSeq ({#toChar message_type}+++req)) sChannel w
	| rpt <> TR_Success
		= (Error "failed to send request", w)
	# (rpt,mbResp,rChannel,w) = receive_MT TIMEOUT rChannel w
	| rpt <> TR_Success
		= (Error "did not receive a response", w)
	# resp = toString (fromJust mbResp)
	# w = closeChannel sChannel (closeRChannel rChannel w)
	= (Ok resp, w)

loadByteCode :: !PrelinkedInterpretationEnvironment !String !*World -> (!MaybeError String (), !*World)
loadByteCode pie file w
	# (mbResp,w) = doRequest LoadNewByteCode file w
	| isError mbResp
		= (liftError mbResp, w)
	# resp = fromOk mbResp
	| resp <> "ok"
		= (Error ("error while loading bytecode: "+++resp), w)

	# init_function = serialize_for_prelinked_interpretation (wrapInitFunction (\_ w -> w)) pie
	# (mbInitResult,w) = doRequest RunInitFunction init_function w
	| isError mbInitResult
		= (liftError mbInitResult, w)
		= (Ok (), w)

runTest :: !PrelinkedInterpretationEnvironment !String !JavaScriptTest !*World -> *World
runTest pie program_name {name,test,finish} w
	# w = output (StartEvent {StartEvent | name=name,location=location}) w

	# serialized_test = serialize_for_prelinked_interpretation
		(\args w -> hyperstrict $ test (is_strict_array args).[0] w)
		pie

	# (start,w) = nsTime w
	# (mbResult,w) = doRequest RunFunction serialized_test w
	| isError mbResult
		= output (EndEvent
			{ emptyEndEvent
			& name     = name
			, location = location
			, event    = Failed $ ?Just $ CustomFailReason (fromError mbResult)
			}) w
	# result = fromOk mbResult

	# (end,w) = nsTime w
	  elapsed = end - start
	  time_ms = 1000 * elapsed.tv_sec + elapsed.tv_nsec / 1000000

	# (result,ok) = deserialize_from_prelinked_interpreter (fromOkWithError (base64DecodeUnique (unsafeCoerce result))) pie
	| ok <> 0
		= output (EndEvent
			{ emptyEndEvent
			& name     = name
			, location = location
			, event    = Failed $ ?Just $ CustomFailReason "result of deserialize was not 0"
			, time     = ?Just time_ms
			}) w
	| otherwise
		= output (EndEvent
			{ emptyEndEvent
			& name     = name
			, location = location
			, event    = finish result
			, time     = ?Just time_ms
			}) w
where
	location = ?Just {moduleName = ?Just program_name}

	is_strict_array :: !{!JSVal} -> {!JSVal}
	is_strict_array arr = arr

runTests :: !String !*World ![JavaScriptTest] -> *World
runTests program_name w tests
	# (node,w) = runProcessIO (IF_WINDOWS "C:\\Program Files\\nodejs\\node.exe" "node") ["client.js"] ?None w
	| isError node
		= abort ("could not start client process: "+++snd (fromError node)+++"\n")
	# (node,_) = fromOk node
	# w = sleep 1 w
	# (mbPie,w) = prepare_prelinked_interpretation (program_name+++".exe") (program_name+++".bc") w
	| isNone mbPie
		= abort "failed to prepare PrelinkedInterpretationEnvironment\n"
	# pie = fromJust mbPie
	# (mbError,w) = loadByteCode pie (program_name+++".pbc") w
	| isError mbError
		= abort (fromError mbError+++"\n")
	# w = seqSt (runTest pie program_name) tests w
	# (_,w) = terminateProcess node w
	= w
where
	sleep :: !Int !*World -> *World
	sleep sec w = snd (IF_WINDOWS (Sleep (sec*1000) w) (sleep sec w))
	where
		Sleep :: !Int !*World -> (!Int,!*World)
		Sleep _ _ = code {
			ccall Sleep "I:I:A"
		}

		sleep :: !Int !*World -> (!Int,!*World)
		sleep _ _ = code {
			ccall sleep "I:I:A"
		}

output :: !TestEvent !*World -> *World
output event w
	# (io,w) = stdio w
	# io = io <<< toJSON event <<< "\n"
	# (_,w) = fclose io w
	| event=:(EndEvent {event=(Failed _)})
		= setReturnCode 1 w
		= w

withGeneratedValues
	:: !Int !GenState !(a -> Bool) !*JSWorld !(a *JSWorld -> (?FailReason, *JSWorld))
	-> (!EndEventType, !*JSWorld)
	| ggen{|*|} a
withGeneratedValues n genState p w testf = test n (ggen{|*|} genState) w
where
	test 0 _ w = (Passed, w)
	test _ [|] w = (Passed, w)
	test i [|v:vs] w
		| not (p v)
			= test i vs w
		# (mbFailure,w) = testf v w
		| isNone mbFailure
			= test (i-1) vs w
			= (Failed mbFailure, w)

expect ::
	!e !a !Relation !(?(a a -> Bool)) !a
	-> EndEventType
	| ==, <, gPrint{|*|} a & gPrint{|*|} e
expect expr x rel mbRelOp y
	| relationOk x rel mbRelOp y
		= Passed
		= Failed $ ?Just $ CounterExamples [
			{ counterExample   = [GPrint (printToString expr)]
			, failedAssertions = [ExpectedRelation (GPrint (printToString x)) rel (GPrint (printToString y))]
			}]

expectJS ::
	!e !a !Relation !(?(a a -> Bool)) !a
	!*JSWorld -> (!?FailReason, !*JSWorld)
	| ==, <, gPrint{|*|} a & gPrint{|*|} e
expectJS expr x rel mbRelOp y w =
	( if (relationOk x rel mbRelOp y)
		?None
		(?Just $ CounterExamples [
			{ counterExample   = [GPrint (printToString expr)]
			, failedAssertions = [ExpectedRelation (GPrint (printToString x)) rel (GPrint (printToString y))]
			}])
	, w
	)

relationOk :: !a !Relation !(?(a a -> Bool)) !a -> Bool | ==, < a
relationOk x _ (?Just op) y = op x y
relationOk x rel _ y = case rel of
	Eq -> x == y
	Ne -> x <> y
	Lt -> x <  y
	Le -> x <= y
	Gt -> x >  y
	Ge -> x >= y
	_  -> abort "relationOk: unknown relation\n"
