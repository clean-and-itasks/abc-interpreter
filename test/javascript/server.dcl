definition module server

from Gast.Gen import :: GenState, generic ggen
from StdOverloaded import class ==, class <
from Testing.TestEvents import :: EndEventType, :: FailReason, :: Relation
from Text.GenPrint import :: PrintState, class PrintOutput, generic gPrint

from ABC.Interpreter.JavaScript import :: JSWorld, :: JSVal

:: JavaScriptTest = E.r:
	{ name   :: !String
	, test   :: !JSVal *JSWorld -> *(r, *JSWorld)
	, finish :: !r -> EndEventType
	}

runTests :: !String !*World ![JavaScriptTest] -> *World

withGeneratedValues
	:: !Int !GenState !(a -> Bool) !*JSWorld !(a *JSWorld -> (?FailReason, *JSWorld))
	-> (!EndEventType, !*JSWorld)
	| ggen{|*|} a

expect ::
	!e !a !Relation !(?(a a -> Bool)) !a
	-> EndEventType
	| ==, <, gPrint{|*|} a & gPrint{|*|} e

expectJS ::
	!e !a !Relation !(?(a a -> Bool)) !a
	!*JSWorld -> (!?FailReason, !*JSWorld)
	| ==, <, gPrint{|*|} a & gPrint{|*|} e
