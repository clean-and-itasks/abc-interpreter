module basic

import Data.Func
import Data.Int
import Data.List
import Data.Real
import Gast.Gen
import StdEnv
import Testing.TestEvents
import Text.GenPrint

import ABC.Interpreter.JavaScript

import server

N_TEST_VALUES :== 10000

myGenState =
	{ genState
	& maxStringLength = 100
	}

allValues :: g -> Bool
allValues _ = True

// These are the limits for integers represented in JavaScript's Number class
intLimit :: !Int -> Bool
intLimit i = IF_INT_64_OR_32 (-9007199254740991 <= i && i <= 9007199254740991) True

realLimit :: !Real -> Bool
realLimit r = -9007199254740991.0 <= r && r <= 9007199254740991.0

Start w = runTests "basic" w
	[ testValueConversion "int-to-int"       intLimit  id         id                          jsValToInt    ?None
	, testValueConversion "string-to-int"    intLimit  id         toString                    jsValToInt    ?None
	, testValueConversion "real-to-int"      intLimit  id         toReal                      jsValToInt    ?None
	, testValueConversion "bool-to-bool"     allValues id         id                          jsValToBool   ?None
	, testValueConversion "int-to-bool"      intLimit  ((<>) 0)   id                          jsValToBool   ?None
	, testValueConversion "real-to-bool"     realLimit (\r -> r <> 0.0) id                    jsValToBool   ?None
		// NB: we cannot use ((<>) 0.0) in the above test. That would create a server-side closure
		// containing a Real, which cannot be copied properly to the client on 32-bit systems.
	, testValueConversion "string-to-bool"   allValues id         (\b -> if b "true" "false") jsValToBool   ?None
	, testValueConversion "string-to-string" allValues id         id                          jsValToString ?None
	, testValueConversion "int-to-string"    intLimit  toString   id                          jsValToString ?None
	, testValueConversion "real-to-string"   realLimit toString   id                          jsValToString (?Just eqRealString)
	, testValueConversion "real-to-real"     realLimit id         id                          jsValToReal   (?Just eqReal)
	, let toBigInt = jsCall (jsGlobal "BigInt") o toString in
	  testValueConversion` "bigint-to-int"   allValues id         toBigInt                    jsValToInt   ?None

	, testInstanceOf "Map is a Map"     "Map" "Map" (jsNew "Map" ()) True
	, testInstanceOf "Set is not a Map" "Map" "Set" (jsNew "Set" ()) False

	, testWrapFunWithResult
	, testGarbageCollection

	, testIssue126
	]
where
	// TODO: these tests are needed to avoid breaking on rounding errors.
	// Ideally we would do something more intelligent than the levenshtein distance of course...
	eqReal x y = abs (x-y) < Epsilon || eqRealString (toString x) (toString y)
	eqRealString x y = levenshtein [c \\ c <-: x] [c \\ c <-: y] <= 3 || abs (toReal x - toReal y) < Epsilon

instance < Bool where (<) x y = toInt x < toInt y
instance toInt Bool where toInt b = if b 1 0

testValueConversion
	:: !String !(g -> Bool) !(g -> v) !(g -> js) !(JSVal -> ?v) !(?(v v -> Bool))
	-> JavaScriptTest
	| ggen{|*|}, gPrint{|*|} g
	& gToJS{|*|} js
	& ==, <, gPrint{|*|} v
testValueConversion name ok genValue genJSValue fromJS mbRelOp =
	testValueConversion` name ok genValue (toJS o genJSValue) fromJS mbRelOp

testValueConversion`
	:: !String !(g -> Bool) !(g -> v) !(g -> JSVal) !(JSVal -> ?v) !(?(v v -> Bool))
	-> JavaScriptTest
	| ggen{|*|}, gPrint{|*|} g
	& ==, <, gPrint{|*|} v
testValueConversion` name ok genValue genJSValue fromJS mbRelOp =
	{ name   = name
	, test   = \_ w -> withGeneratedValues N_TEST_VALUES myGenState ok w test
	, finish = id
	}
where
	test v w
		# (v`,w) = jsForceFetch (genJSValue v) w
		= case fromJS v` of
			?None -> (?Just Crashed, w)
			?Just v` -> expectJS v (genValue v) Eq mbRelOp v` w

testInstanceOf :: !String !String !String !(*JSWorld -> (JSVal, *JSWorld)) !Bool -> JavaScriptTest
testInstanceOf name cls val new expected =
	{ name   = "instanceof: " +++ name
	, test   = test
	, finish = \r -> expect val r Eq ?None expected
	}
where
	test _ w
		# (v,w) = new w
		= v jsInstanceOf cls .?? (False, w)

testWrapFunWithResult :: JavaScriptTest
testWrapFunWithResult =
	{ name   = "jsWrapFunWithResult gives a result"
	, test   = test
	, finish = \r -> expect 42 r Eq ?None 42
	}
where
	test me w
		# (f,w) = jsWrapFunWithResult f me w
		= f .$? () $ (0, w)

	f _ w = (toJS 42, w)

/* The benchmark test suite covers the general gc algorithm well. Here, we only
 * check that `JSRef`s found on the Clean heap are communicated to the
 * JavaScript wrapper through `js_ref_found`, so that JavaScript values shared
 * with Clean remain shared even after garbage collection runs. */
testGarbageCollection :: JavaScriptTest
testGarbageCollection =
	{ name   = "garbage collection: JSRefs are managed correctly"
	, test   = test
	, finish = \r -> expect () r Eq ?None 42
	}
where
	test _ w
		# (obj,w) = jsNew "Object" (jsRecord ["x" :> 42]) w
		// trigger garbage collection by reversing a list many times
		# obj = hd $ iter 1000 reverse $ repeatn 1000 obj
		= obj .# "x" .?? (0, w)

testIssue126 :: JavaScriptTest
testIssue126 =
	{ name = "toString of JSInt SmallestInt (issue #126)"
	, test = \_ w -> (toString (toJS SmallestInt), w)
	, finish = \r -> expect () r Eq ?None (toString SmallestInt)
	}
