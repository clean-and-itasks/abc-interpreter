const assert=require ('assert');
const fs=require ('fs');
const net=require ('net');
const process=require ('process');

const {ABCInterpreter,SharedCleanValue}=require ('../../lib/WebPublic/js/abc-interpreter.js');

global.fetch=path => new Promise ((resolve,reject) => {
	fs.readFile (path,(err,data) => {
		if (err)
			reject (err);

		resolve ({
			ok: true,
			arrayBuffer: () => data.buffer
		});
	});
});

ABC=null;

function load_abc (bytecode_path)
{
	return ABCInterpreter.instantiate ({
		bytecode_path: bytecode_path,
		util_path: '../../lib/WebPublic/js/abc-interpreter-util.wasm',
		interpreter_path: '../../lib/WebPublic/js/abc-interpreter.wasm',
		heap_size: 2<<20,
		stack_size: 512<<10,
		with_js_ffi: true,
		encoding: 'utf-8',

		interpreter_imports: {
			debug_instr: (addr,instr) => {
				console.log ((addr/8-ABC.code_offset)+'\t'+ABCInterpreter.instructions[instr]);
			},
			illegal_instr: (addr,instr) => {
				assert.fail ('illegal instruction '+instr+' ('+ABCInterpreter.instructions[instr]+') at address '+(addr/8-ABC.code_offset-ABC.code_offset-ABC.code_offset-ABC.code_offset-ABC.code_offset-ABC.code_offset-ABC.code_offset-ABC.code_offset-ABC.code_offset));
			},
			out_of_memory: () => {
				assert.fail ('out of memory');
			},

			putchar: v => {
				process.stdout.write (String.fromCharCode (v));
			},
			print_int: (high,low) => {
				if (high==0 && low>=0) {
					process.stdout.write (String (low));
				} else {
					var n=BigInt (high)*2n**32n;
					if (low<0) {
						n+=2n**31n;
						low+=2**31;
					}
					n+=BigInt (low);
					process.stdout.write (String (n));
				}
			},
			print_bool: v => {
				process.stdout.write (v==0 ? 'False' : 'True');
			},
			print_char: v => {
				process.stdout.write ("'"+String.fromCharCode (v)+"'");
			},
			print_real: v => {
				process.stdout.write (Number (0+v).toLocaleString (
					['en-US'],
					{
						useGrouping: false,
						maximumSignificantDigits: 15,
					}
				));
			},
		}
	});
}

const socket=net.createServer (connection => {
	connection.shared_clean_values=null;

	connection.on ('data',data => {
		const message_type=data[0];

		switch (message_type)
		{
			case 0: /* load new bytecode */
				socket.abc_promise=load_abc (data.toString().slice (1));
				socket.abc_promise.then (abc => {
					socket.abc=abc;
					connection.write ('ok');
				}).catch (e => {
					connection.write (e.toString());
				});
				break;
			case 1: { /* run init function */
				ABC=socket.abc;

				const node=ABC.deserialize (new Uint8Array (data.buffer,1));
				const ref=ABC.share_clean_value (node,connection);
				ABC.interpret (new SharedCleanValue (ref),[socket,1]);
				ABC.clear_shared_clean_value (ref);

				connection.write ('ok');
				break; }
			case 2: { /* run function */
				ABC=socket.abc;

				const node=ABC.deserialize (new Uint8Array (data.buffer,1));
				const ref=ABC.share_clean_value (node,connection);
				ABC.interpret (new SharedCleanValue (ref),[connection]);
				ABC.clear_shared_clean_value (ref);

				const asp=ABC.interpreter.instance.exports.get_asp();
				const hp_ptr=ABC.memory_array[asp/4+2];
				const result_ptr=ABC.memory_array[hp_ptr/4+2];
				const serialized_result=ABC.copy_to_string (result_ptr);

				connection.write (serialized_result);
				break; }
		}
	});
});
socket.listen (20519,() => {
	console.log ('listening...');
});
