module serialization

import Data.Func
import Data.GenEq
import Data.GenLexOrd
import Data.Maybe
import Data.Tuple
import Gast.Gen
import StdDynamic
import StdEnv
import _SystemStrictMaybes
import Testing.TestEvents
from Text import concat3
import Text.GenPrint

import ABC.Interpreter.JavaScript

import server

Start w = runTests "serialization" w
	[ testBasicValue (37, False, "string", 'a')
	, testBasicValue (-1, True, "", '~')
	, testBasicValue "abcd"
	, testBasicValue "abcdefgh"
	, testBasicValue "abcdefghijkl"
	// NB: we cannot test Reals because they cannot be copied from a 32-bit server
	, testBasicValue [1,2,3]
	, testBasicValue [!1,2,3]
	, testBasicValue [1,2,3!]
	, testBasicValue [!1,2,3!]
	, testBasicValue [#1,2,3]
	, testBasicValue [#1,2,3!]
	, testBasicValue (?Just  False)
	, testBasicValue (?#Just 123)
	, testBasicValue (?^Just "string")
	, testBasicValue (is_normal_array {1,2,3})
	, testBasicValue {!1,2,3}
	, testBasicValue ([1], [1])
	, testBasicValue {#True, False, True, False, True}
	]

testBasicValue :: !a -> JavaScriptTest | ==, <, gPrint{|*|}, TC a
testBasicValue val = testGraphCopy
	(concat3 (toString (typeCodeOfDynamic (dynamic val))) ": " (printToString val))
	val
	tuple
	(\r -> expect r r Eq ?None val)

testGraphCopy :: !String !a !(a *JSWorld -> (b, *JSWorld)) !(b -> EndEventType) -> JavaScriptTest
testGraphCopy name val jsFun finish =
	{ name   = name
	, test   = \_ -> jsFun val
	, finish = finish
	}

is_normal_array :: !{a} -> {a}
is_normal_array xs = xs

instance < Bool where (<) x y = toInt x < toInt y
instance toInt Bool where toInt b = if b 1 0

instance == (a,b,c,d) | == a & == b & == c & == d derive gEq
instance < (a,b,c,d) | < a & < b & < c & < d
where
	(<) x y = (gLexOrd{|*->*->*->*->*|} ord ord ord ord x y) =: LT
	where
		ord x y = if (x < y) LT (if (y < x) GT EQ)

instance == [!a] | == a derive gEq
instance < [!a] | Ord a
where
	(<) x y = [e \\ e <|- x] < [e \\ e <|- y]

instance == [a!] | == a derive gEq
instance < [a!] | Ord a
where
	(<) x y = [e \\ e <|- x] < [e \\ e <|- y]

instance == [!a!] | == a derive gEq
instance < [!a!] | Ord a
where
	(<) x y = [e \\ e <|- x] < [e \\ e <|- y]

instance == [#a] | UList, == a derive gEq
instance < [#a] | UList, Ord a
where
	(<) x y = [e \\ e <|- x] < [e \\ e <|- y]

instance == [#a!] | UTSList, == a derive gEq
instance < [#a!] | UTSList, Ord a
where
	(<) x y = [e \\ e <|- x] < [e \\ e <|- y]

instance < (?^a) | Ord a where (<) x y = maybeToList x < maybeToList y
instance < (?#a) | UMaybe, Ord a where (<) x y = maybeToList x < maybeToList y

instance == {a} | == a where (==) xs ys = [x \\ x <-: xs] == [y \\ y <-: ys]
instance < {a} | Ord a where (<) xs ys = [x \\ x <-: xs] < [y \\ y <-: ys]

instance == {!a} | Array {!} a & == a where (==) xs ys = [x \\ x <-: xs] == [y \\ y <-: ys]
instance < {!a} | Array {!} a & Ord a where (<) xs ys = [x \\ x <-: xs] < [y \\ y <-: ys]

instance == {#Bool} where (==) xs ys = [x \\ x <-: xs] == [y \\ y <-: ys]
instance < {#Bool} where (<) xs ys = [x \\ x <-: xs] < [y \\ y <-: ys]
gPrint{|{#Bool}|} xs st = gPrint{|*|} {!x \\ x <-: xs} st
