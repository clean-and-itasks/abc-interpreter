FROM cleanlang/nitrile:0.4

RUN \
	dpkg --add-architecture i386 &&\
	apt-get update -qq &&\
	apt-get install -y -qq --no-install-recommends\
		bc\
		build-essential\
		ca-certificates\
		clang\
		git\
		gpg\
		libncurses-dev\
		libncurses-dev:i386\
		time\
		wabt

# Node, for the JavaScript FFI tests
RUN \
	curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg &&\
	echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list &&\
	apt-get install -qq -y nodejs
